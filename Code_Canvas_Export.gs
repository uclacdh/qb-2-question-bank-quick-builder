/*
(QB)² - Question Bank Quick Builder

The Clear BSD License

Copyright (c) 2020 Regents of the University of California
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted (subject to the limitations in the disclaimer
below) provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

     * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     * Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

// global namespace that each XmlService.createElement() created tag NEEDS to have as a second parameter
var namespace = XmlService.getNamespace("http://www.imsglobal.org/xsd/ims_qtiasiv1p2");

function exportCanvas(rootElement, replace, activeSheetName) {
    showCompilingMessage('Compiling Canvas XML...', 'This may take a few minutes...', 350);
    exportSpreadsheetCanvas(rootElement, replace, activeSheetName);
}

//helper export function that builds XML content
function exportSpreadsheetCanvas(rootElement, replaceFile, activeSheetName) {

    var properties = PropertiesService.getDocumentProperties();
    var shuffle = properties.getProperty('shuffleCheckbox');
    if (shuffle == null) {
        shuffle = true;
        properties.setProperty('shuffleCheckbox', true);
    } else {
        shuffle = (shuffle == "true");
    }

    //We export from the active sheet, but this is undesirable if the active sheet is not a question bank
    //Let the user know they need to change sheets if they are on one of the following
    if (activeSheetName == 'Categories' || activeSheetName == 'QuestionTypes' || activeSheetName == 'QuestionOptions' || activeSheetName == 'Import Errors') {
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
            'Switch to Question Bank',
            'Export is performed on the sheet that is currently visible.\nPlease navigate to your question bank and try again.',
            ui.ButtonSet.OK);
        return;
    }
    var spreadsheet = SpreadsheetApp.getActive();
    var sheet = spreadsheet.getSheetByName(activeSheetName);
    var fileName = spreadsheet.getName() + ".xml";

    //create root element of XML file
    //rootElement is <questestinterop>
    var rootXML = '<questestinterop xmlns="http://www.imsglobal.org/xsd/ims_qtiasiv1p2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.imsglobal.org/xsd/ims_qtiasiv1p2 http://www.imsglobal.org/xsd/ims_qtiasiv1p2p1.xsd"></questestinterop>';
    var rootDoc = XmlService.parse(rootXML);
    var root = rootDoc.detachRootElement();

    var range = sheet.getDataRange();
    var values = range.getValues();
    var numRows = range.getNumRows();

    var unsupportedQuestionsCaught = [];

    var unsupportedFeedbackCaught = false;

    try {
        //Catch errors at the sheet level to provide a popup letting the user know export failed.
        //r = 2 because first two rows are reserved for column header and extra space for notes that arent needed for compiling.
        for (var r = 2; r < numRows; r++) {
            try {
                //Catch errors at the question level so we can provide users with the line they occur on

                // Get question type
                var questionType = values[r][1];
                var continueLoop = false;

                var maxAnswers = 6;
                // strip feedback from answers, starting at index 3 and ending at 3 + (maxAnswers * 2).
                for(i = 0; i < maxAnswers; i++) {

                    var answer = values[r][(i * 2) + 3];

                    if(answer == true || answer == false) {
                        continue;
                    }

                    values[r][(i * 2) + 3] = stripFeedback(values[r][(i * 2) + 3]);

                    if(unsupportedFeedbackCaught == false & answer != values[r][(i * 2) + 3]) {
                        unsupportedFeedbackCaught = true;
                    }
                }

                // what type of question should be built based on the questiontype
                switch(questionType) {
                    case "multichoice":
                        var addToRoot = makeChoiceQuestion(values[r], r, shuffle);
                        break;
                    case "truefalse":
                        var addToRoot = makeChoiceQuestion(values[r], r, shuffle);
                        break;
                    case "multianswer":
                        var addToRoot = makeChoiceQuestion(values[r], r, shuffle);
                        break;
                    case "poodllrecording":
                        var addToRoot = makeEssayQuestion(values[r], r);
                        if(unsupportedQuestionsCaught.indexOf(questionType) == -1) {
                            unsupportedQuestionsCaught.push(questionType);
                        }
                        break;
                    case "essay":
                        var addToRoot = makeEssayQuestion(values[r], r);
                        break;
                    case "shortanswer":
                        var addToRoot = makeShortAnswerQuestion(values[r], r);
                        break;
                    case "description":
                        var addToRoot = makeDescription(values[r]);
                        break;
                    case "matching":
                        var addToRoot = makeMatchingQuestion(values[r], shuffle);
                        break;
                    case "missingwords":
                        var addToRoot = makeMissingwordsQuestion(values[r], shuffle);
                        break;
                    case "dragdrop": // cannot replicate dragdrop XML at this time, but same formatting between missingwords and dragdrop allows question type reassignment.
                        var addToRoot = makeMissingwordsQuestion(values[r], shuffle);
                        if(unsupportedQuestionsCaught.indexOf(questionType) == -1) {
                            unsupportedQuestionsCaught.push(questionType);
                        }
                        break;
                    case "cloze":
                        if(unsupportedQuestionsCaught.indexOf(questionType) == -1) {
                            unsupportedQuestionsCaught.push(questionType);
                        }
                        continueLoop = true;
                        break;
                    case "ordering":
                        var addToRoot = makeOrderingQuestion(values[r], shuffle);
                        break;
                    default:
                        continueLoop = true;
                        break;
                }

                if(continueLoop) {
                    continueLoop = false;
                    continue;
                }

                addToRoot.setAttribute("title", values[r][2].substring(0, 44));
                root.addContent(addToRoot);


            }

            catch (e) {
                e = e.toString(); //Cast to string in case the exception is not of type string
                var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' + e;
                //Variable height based on error length
                var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(50 + 15 * Math.ceil((e.length - 67) / 67));
                SpreadsheetApp.getUi().showModalDialog(html, 'Canvas Export Failed');
            }

        }

        // if there are any unsupported questions brought into Canvas export, throw an error message.
        if(unsupportedQuestionsCaught.length > 0) {

            var unsupportedQuestionMessage = "";

            // build the error message
            for(var i = 0; i < unsupportedQuestionsCaught.length; i++) {

                if(unsupportedQuestionsCaught.length == 1) {
                    unsupportedQuestionMessage = unsupportedQuestionsCaught[i];
                }

                // fixing string to work for more than one question type
                else if(i != unsupportedQuestionsCaught.length - 1) {

                    // no need for a comma if there's only two questions
                    if(unsupportedQuestionsCaught.length != 2) {
                        var comma = ", ";
                    }

                    else {
                        var comma = " ";
                    }
                    unsupportedQuestionMessage = unsupportedQuestionMessage + unsupportedQuestionsCaught[i] + comma;
                }

                else {
                    unsupportedQuestionMessage = unsupportedQuestionMessage + " and " + unsupportedQuestionsCaught[i];
                }

            }

            unsupportedQuestionMessage = "The question type(s) " + unsupportedQuestionMessage + " are not supported for Canvas (QTI) and will not be included in the export.";

            // poodllrecording questions are supported, but turned into essay questions
            if(unsupportedQuestionsCaught.indexOf("poodllrecording") != -1) {
                unsupportedQuestionMessage = unsupportedQuestionMessage + "\n\n" + "Because Canvas has recorded microphone responses integrated into their essay questions, the poodllrecording question type is imported into Canvas as an essay question.";
            }

            // poodllrecording questions are supported, but turned into essay questions
            if(unsupportedQuestionsCaught.indexOf("dragdrop") != -1) {
                unsupportedQuestionMessage = unsupportedQuestionMessage + "\n\n" + "Dragdrop is not currently supported for QTI XML export, but it can be exported to Canvas as a missingwords question.";
            }

            var ui = SpreadsheetApp.getUi();
            var result = ui.alert(
            "Unsupported Question Types Found",
            unsupportedQuestionMessage,
            ui.ButtonSet.OK_CANCEL);

            // if user presses cancel or closes the alert window (presses the X on the top right corner), end the function
            if (result == ui.Button.CANCEL || result == ui.Button.CLOSE) {
                return(0);
            }

            else {
            showCompilingMessage('Compiling Canvas XML...', 'This may take a few minutes...', 350);
            }

        }

        if(unsupportedFeedbackCaught) {

            unsupportedFeedbackMessage = "Feedback is not supported in Canvas export. It will be trimmed away in your answers.";

            var ui = SpreadsheetApp.getUi();
            var result = ui.alert(
            "Unsupported Feedback Found",
            unsupportedFeedbackMessage,
            ui.ButtonSet.OK_CANCEL);

            // if user presses cancel or closes the alert window (presses the X on the top right corner), end the function
            if (result == ui.Button.CANCEL || result == ui.Button.CLOSE) {
                return(0);
            }

            else {
                showCompilingMessage('Compiling Canvas XML...', 'This may take a few minutes...', 350);
            }

        }

        try {
            //Creates an XML document with <quiz> as the root node
            var document = XmlService.createDocument(root);
            //Formats the XML into a human-readable XML document
            var rawValue = XmlService.getPrettyFormat().setOmitDeclaration(true).format(document);

            // create manifest file to sit alongside quetsion bank XML file in folder
            var manifestFile = makeManifestFile(fileName);

            //writes XML document to Google Drive
            exportDocumentCanvas(fileName, manifestFile, rawValue, ContentService.MimeType.XML, replaceFile);
        } catch (e) {
            throw "An error occurred while writing the XML sheet to your Google Drive.";
        }

    } catch (e) {
        e = e.toString(); //Cast to string in case the exception is not of type string
        var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' + e;
        //Variable height based on error length
        var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(50 + 15 * Math.ceil((e.length - 67) / 67));
        SpreadsheetApp.getUi().showModalDialog(html, 'Canvas Export Failed');
    }
}

// create manifest XML file that gives metadata to Canvas. Needed for compatability with new quizzes
function makeManifestFile(fileName) {

    // manifest namespace that children need to inherit.
    var manifestNamespace = XmlService.getNamespace("http://www.imsglobal.org/xsd/imsccv1p1/imscp_v1p1");

    // the <manifest> tag has all of the metadata for which version of QTI to use
    var rootXML = '<manifest xmlns="http://www.imsglobal.org/xsd/imsccv1p1/imscp_v1p1" xmlns:lom="http://ltsc.ieee.org/xsd/imsccv1p1/LOM/resource" xmlns:imsmd="http://www.imsglobal.org/xsd/imsmd_v1p2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.imsglobal.org/xsd/imsccv1p1/imscp_v1p1 http://www.imsglobal.org/xsd/imscp_v1p1.xsd http://ltsc.ieee.org/xsd/imsccv1p1/LOM/resource http://www.imsglobal.org/profile/cc/ccv1p1/LOM/ccv1p1_lomresource_v1p0.xsd http://www.imsglobal.org/xsd/imsmd_v1p2 http://www.imsglobal.org/xsd/imsmd_v1p2p2.xsd"></manifest>';
    var rootDoc = XmlService.parse(rootXML);
    var root = rootDoc.detachRootElement();


    // the <resource> tag has the path to the file with the actual question bank.
    var resourcesElement = XmlService.createElement('resources', manifestNamespace);

    var resourceElement = XmlService.createElement('resource', manifestNamespace);
    resourceElement.setAttribute('identifier', fileName.slice(0,-4));
    resourceElement.setAttribute('type', 'imsqti_xmlv1p2');

    var fileElement = XmlService.createElement('file', manifestNamespace);
    fileElement.setAttribute('href', fileName);

    resourceElement.addContent(fileElement);
    resourcesElement.addContent(resourceElement);
    root.addContent(resourcesElement);

    var document = XmlService.createDocument(root);
    var rawValue = XmlService.getPrettyFormat().setOmitDeclaration(true).format(document);

    return(rawValue);

}

//creates a zip file from content in Google Drive
function exportDocumentCanvas(filename, manifestFileContent, content, type, replaceFile) {

    //Creates an XML file and a folder, and zips the XML files into the folder
    var file = DriveApp.createFile(filename, content);
    var manifestFile = DriveApp.createFile("imsmanifest.xml", manifestFileContent);
    var zipFolder = DriveApp.createFolder(filename.slice(0,-4));
    zipFolder.addFile(file);
    zipFolder.addFile(manifestFile);
    var zipFile = DriveApp.createFile(Utilities.zip(getBlobs(zipFolder, ''), filename.slice(0,-4) + '.zip'));
    
    // delete original file and open folder, set file to zipped file
    file.setTrashed(true);
    zipFolder.setTrashed(true);
    manifestFile.setTrashed(true);
    file = zipFile;

    var currentFileId = SpreadsheetApp.getActive().getId();
    var currentFiles = DriveApp.getFilesByName(SpreadsheetApp.getActive().getName());
    var currentFile;

    while (currentFiles.hasNext()) {

        currentFile = currentFiles.next();

        if (currentFile.getId() == currentFileId) break;
    }

    var folders = currentFile.getParents();

    var parentFolder;
    while (folders.hasNext()) {
        parentFolder = folders.next();
    }

    if (parentFolder != null) {
        var newFile;

        if (replaceFile) {

            newFile = file.makeCopy(file.getName(), parentFolder);
            file.setTrashed(true);

            currentFiles = parentFolder.getFiles();

            while (currentFiles.hasNext()) {

                currentFile = currentFiles.next();

                if (currentFile.getName() == newFile.getName() && currentFile.getId() != newFile.getId()) {

                    currentFile.setTrashed(true);
                }
            }
        } else {
            newFile = file.makeCopy(file.getName(), parentFolder);
            file.setTrashed(true);
        }

        var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' +
            '<style>.display { width:355px; height:85px; text-align: center; overflow: auto; } </style>' +
            'File exported successfully. You can view the file here:' +
            '<div class="display"><br><br><a href="' + newFile.getUrl() + '" target="_blank">' + newFile.getName() + '</a></div>';

        var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(100);

        SpreadsheetApp.getUi().showModalDialog(html, 'Moodle Export Complete!');
    } else {
        //Could not access containing folder for writing so leave it in user's root
        var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' +
            '<style>.display { width:355px; height:95px; text-align: center; overflow: auto; } </style>' +
            'File exported successfully. Since you do not appear to be the owner of the spreadsheet, the file ' +
            'has been saved in your own Google Drive. You can view the file here:' +
            '<div class="display"><br><br><a href="' + file.getUrl() + '" target="_blank">' + file.getName() + '</a></div>';

        var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(130);

        SpreadsheetApp.getUi().showModalDialog(html, 'Moodle Export Complete!');
    }
}

// create material and mattext element, often used to display text (in this case, the value parameter)
function setMattext(value) {
    value = String(value); //cast to string in case spreadsheet cell is of type number

    var materialElement = XmlService.createElement('material', namespace);
    var mattextElement = XmlService.createElement('mattext', namespace);

    mattextElement.setText(value);

    materialElement.addContent(mattextElement);
    return(materialElement);
}

// used to set metadata for the question type, returns an 'itemmetadata' tag
function setQuestionTypeMetaData(questionType) {
    
    var itemmetadataElement = XmlService.createElement('itemmetadata', namespace); // question metadata tag
    var qtimetadataElement = XmlService.createElement('qtimetadata', namespace);
    var qtimetadatafieldElement = XmlService.createElement('qtimetadatafield', namespace);
    var fieldlabelElement = XmlService.createElement('fieldlabel', namespace); // tag that dictates behavior of fieldentry
    var fieldentryElement = XmlService.createElement('fieldentry', namespace); // in this case, the tag that holds the question type

    var textToSet;
    switch(questionType) {
        case "multichoice":
            textToSet = 'multiple_choice_question';
            break;
        case "truefalse":
            textToSet = 'true_false_question';
            break;
        case "multianswer":
            textToSet = 'multiple_answers_question';
            break;
        case "essay":
            textToSet = "essay_question";
            break;
        case "poodllrecording":
            textToSet = "essay_question";
            break;
        case "shortanswer":
            textToSet = "short_answer_question";
            break;
        case "description":
            textToSet = "text_only_question";
            break;
        case "matching":
            textToSet = "matching_question";
            break;
        case "missingwords":
            textToSet = "multiple_dropdowns_question";
            break;
        case "dragdrop":
            textToSet = "multiple_dropdowns_question";
            break;
        case "ordering":
        	textToSet = "ordering_question";
    }

    fieldlabelElement.setText('question_type');
    fieldentryElement.setText(textToSet);

    qtimetadatafieldElement.addContent(fieldlabelElement);
    qtimetadatafieldElement.addContent(fieldentryElement);
    qtimetadataElement.addContent(qtimetadatafieldElement);
    return(itemmetadataElement.addContent(qtimetadataElement));
}

// used to generate multiple choice, multiple answer, and true/false questions.
// answers with point values of 100 are the correct answers, unless questionType is multianswer, then all nonzero values are correct
function makeChoiceQuestion(questionArray, row, shuffle) {

    var question = questionArray[2];
    var questionType = questionArray[1];

    var itemElement = XmlService.createElement('item', namespace);
    itemElement.addContent(setQuestionTypeMetaData(questionType));

    var presentationElement = XmlService.createElement('presentation', namespace);

    // actual question is set here
    var nameTextElement = setMattext(question);
    presentationElement.addContent(nameTextElement);

    // ID of question is set here
    var responselidElement = XmlService.createElement('response_lid', namespace);
    responselidElement.setAttribute('ident', 'Q' + (row - 1));

    if(questionType == 'multianswer') {
        var cardinality = 'Multiple';
    }

    else {
        var cardinality = 'Single';
    }

    responselidElement.setAttribute('rcardinality', cardinality);
    responselidElement.setAttribute('rtiming', 'No');

    var renderchoiceElement = XmlService.createElement('render_choice', namespace);

    if(shuffle) {
        renderchoiceElement.setAttribute("shuffle", "yes");
    }

    var flowlabelElement = XmlService.createElement('flow_label', namespace);

    var correctAnswerIndexes = []; // save index of answer with point value of 100
    var incorrectAnswerIndexes = []; // save index of all other answers; this is used primarily for multianswer <not> tag elements
    var maxAnswers = 6;
    // set all possible answers, up to six.
    for(var i = 0; i < maxAnswers; i++) {

        // captures each answer and point value group
        var answerString = String(questionArray[3 + (i * 2)]); // cast to string to prevent issues with answers as numbers/bools
        var pointValue = questionArray[4 + (i * 2)];

        if(answerString == "" || answerString == null) { // if there's no answer, break
            break;
        }

        if(answerString == 'true' || answerString == 'false') { 
            answerString = answerString.toUpperCase(); // done to prevent string cast of bool being lowercase, for looks only
        }

        if(pointValue == 100 || (questionType == 'multianswer' && pointValue != 0)) {
            correctAnswerIndexes.push(i + 1); // one added because answer tags are one-indexed.
        }

        else {
            incorrectAnswerIndexes.push(i + 1); // one added because answer tags are one-indexed.
        }

        // response label element gives ID to each answer, which is pointed to to find correct answer
        var responselabelElement = XmlService.createElement('response_label', namespace);
        responselabelElement.setAttribute('ident', 'Answer' + (i + 1));
        responselabelElement.addContent(setMattext(answerString));
        flowlabelElement.addContent(responselabelElement);

    }

    renderchoiceElement.addContent(flowlabelElement);
    responselidElement.addContent(renderchoiceElement);
    presentationElement.addContent(responselidElement);
    itemElement.addContent(presentationElement);

    var resprocessingElement = XmlService.createElement('resprocessing', namespace);
    var outcomesElement = XmlService.createElement('outcomes', namespace);
    
    var decvarElement = XmlService.createElement('decvar', namespace);
    decvarElement.setAttribute('minvalue', '0');
    decvarElement.setAttribute('maxvalue', '1');
    outcomesElement.addContent(decvarElement);
    resprocessingElement.addContent(outcomesElement);

    var respconditionElement = XmlService.createElement('respcondition', namespace);
    respconditionElement.setAttribute('title', 'Correct');
    var conditionvarElement = XmlService.createElement('conditionvar', namespace);

    var andElement = XmlService.createElement('and', namespace);

    // set right answers
    for(var i = 0; i < correctAnswerIndexes.length; i++) {
    
        // the text inside of here determines the answers.
        var varequalElement = XmlService.createElement('varequal', namespace);
        varequalElement.setAttribute('respident', 'Q' + (row - 1));

        var answer = 'Answer' + correctAnswerIndexes[i]; // set answer's ID
        varequalElement.setText(answer);
        andElement.addContent(varequalElement);

    }

    // set wrong answers
    for(var i = 0; i < incorrectAnswerIndexes.length; i++) {

        var notElement = XmlService.createElement('not', namespace);

        // the text inside of here determines the wrong answers.
        var varequalElement = XmlService.createElement('varequal', namespace);
        varequalElement.setAttribute('respident', 'Q' + (row - 1));

        var answer = 'Answer' + incorrectAnswerIndexes[i]; // set answer's ID
        varequalElement.setText(answer);

        notElement.addContent(varequalElement)
        andElement.addContent(notElement);

    }

    conditionvarElement.addContent(andElement);
    respconditionElement.addContent(conditionvarElement);

    var setvarElement = XmlService.createElement('setvar', namespace);
    setvarElement.setAttribute('action', 'Set');
    setvarElement.setText('1');
    respconditionElement.addContent(setvarElement);
    resprocessingElement.addContent(respconditionElement);

    itemElement.addContent(resprocessingElement);
    return(itemElement);

}

// makes essay/poodl questions.
function makeEssayQuestion(questionArray, row) {

    var question = questionArray[2];
    var questionType = questionArray[1];

    var itemElement = XmlService.createElement('item', namespace);
    itemElement.addContent(setQuestionTypeMetaData(questionType));

    var presentationElement = XmlService.createElement('presentation', namespace);

    var nameTextElement = setMattext(question);
    presentationElement.addContent(nameTextElement);

    var responsestrElement = XmlService.createElement('response_str', namespace);
    responsestrElement.setAttribute('ident', 'Q' + (row - 1));
    responsestrElement.setAttribute('rcardinality','Single');

    var renderfibElement = XmlService.createElement('render_fib', namespace);
    var responselabelElement = XmlService.createElement('response_label', namespace);
    responselabelElement.setAttribute('ident', 'answer' + (row-1));
    responselabelElement.setAttribute('rshuffle', "No");

    renderfibElement.addContent(responselabelElement);
    responsestrElement.addContent(renderfibElement);
    presentationElement.addContent(responsestrElement);

    var resprocessingElement = XmlService.createElement('resprocessing', namespace);
    var outcomesElement = XmlService.createElement('outcomes', namespace);
    
    var decvarElement = XmlService.createElement('decvar', namespace);
    decvarElement.setAttribute('minvalue', '0');
    decvarElement.setAttribute('maxvalue', '100');
    decvarElement.setAttribute('varname', 'SCORE');
    decvarElement.setAttribute('vartype', "Decimal");

    outcomesElement.addContent(decvarElement);
    resprocessingElement.addContent(outcomesElement);

    var respconditionElement = XmlService.createElement('respcondition', namespace);
    respconditionElement.setAttribute('continue', 'No');
    var conditionvarElement = XmlService.createElement('conditionvar', namespace);

    respconditionElement.addContent(conditionvarElement);
    resprocessingElement.addContent(respconditionElement);
    
    itemElement.addContent(presentationElement);
    itemElement.addContent(resprocessingElement);
    return(itemElement);

}

function makeShortAnswerQuestion(questionArray, row) {

    var question = questionArray[2];
    var questionType = questionArray[1];

    var itemElement = XmlService.createElement('item', namespace);
    itemElement.addContent(setQuestionTypeMetaData(questionType));

    var presentationElement = XmlService.createElement('presentation', namespace);

    var nameTextElement = setMattext(question);
    presentationElement.addContent(nameTextElement);

    var responsestrElement = XmlService.createElement('response_str', namespace);
    responsestrElement.setAttribute('ident', 'Q' + (row - 1));
    responsestrElement.setAttribute('rcardinality','Single');

    var renderfibElement = XmlService.createElement('render_fib', namespace);
    var responselabelElement = XmlService.createElement('response_label', namespace);
    responselabelElement.setAttribute('ident', 'answer' + (row-1));
    responselabelElement.setAttribute('rshuffle', "No");

    renderfibElement.addContent(responselabelElement);
    responsestrElement.addContent(renderfibElement);
    presentationElement.addContent(responsestrElement);

    itemElement.addContent(presentationElement);

    var resprocessingElement = XmlService.createElement('resprocessing', namespace);
    var outcomesElement = XmlService.createElement('outcomes', namespace);
    
    var decvarElement = XmlService.createElement('decvar', namespace);
    decvarElement.setAttribute('minvalue', '0');
    decvarElement.setAttribute('maxvalue', '100');
    decvarElement.setAttribute('varname', 'SCORE');
    decvarElement.setAttribute('vartype', "Decimal");

    outcomesElement.addContent(decvarElement);
    resprocessingElement.addContent(outcomesElement);

    var respconditionElement = XmlService.createElement('respcondition', namespace);
    respconditionElement.setAttribute('continue', 'No');
    var conditionvarElement = XmlService.createElement('conditionvar', namespace);

    var maxAnswers = 6;
    var correctAnswers = [];
    // set all possible answers, up to six.
    for(var i = 0; i < maxAnswers; i++) {

        // captures each answer and point value group
        var answerString = String(questionArray[3 + (i * 2)]); // cast to string to prevent issues with answers as numbers/bools
        var pointValue = questionArray[4 + (i * 2)];

        if(answerString == "" || answerString == null) { // if there's no answer, break
            break;
        }

        if(pointValue == 100) {
            correctAnswers.push(answerString); // one added because answer tags are one-indexed.
        }

    }

    for(var i = 0; i < correctAnswers.length; i++) {

        var varequalElement = XmlService.createElement('varequal', namespace);
        varequalElement.setAttribute('respident', 'Q' + (row - 1));
        varequalElement.setText(correctAnswers[i]);
        conditionvarElement.addContent(varequalElement);

    }

    respconditionElement.addContent(conditionvarElement);

    var setvarElement = XmlService.createElement('setvar', namespace);
    setvarElement.setAttribute('action', 'Set');
    setvarElement.setAttribute('varname', 'SCORE');
    setvarElement.setText('100');
    respconditionElement.addContent(setvarElement);
    resprocessingElement.addContent(respconditionElement);

    itemElement.addContent(resprocessingElement);
    return(itemElement);

}

// creates a stimulus in new quizzes
// a question must be attached to the stimulus, so it doesn't work on its own.
function makeDescription(questionArray) {

    var question = questionArray[2];
    var questionType = questionArray[1];

    var itemElement = XmlService.createElement('item', namespace);
    itemElement.setAttribute("title", "Attention"); // the second attribute in this tag will be the header of the stimulus
    itemElement.addContent(setQuestionTypeMetaData(questionType));

    var presentationElement = XmlService.createElement('presentation', namespace);
    presentationElement.addContent(setMattext(question));
    itemElement.addContent(presentationElement);

    return(itemElement);

}

// creates matching questions.
function makeMatchingQuestion(questionArray, shuffle) {

    var question = questionArray[2];
    var questionType = questionArray[1];

    var itemElement = XmlService.createElement('item', namespace);
    itemElement.addContent(setQuestionTypeMetaData(questionType));

    var presentationElement = XmlService.createElement('presentation', namespace);
    presentationElement.addContent(setMattext(question));

    var maxAnswers = 6; // set all possible answers, up to six.

    var respconditionElements = []; // stores the XML for each correct answer matchup.
    // this is needed because even though the left and right matches are set in the for loop in the <presentation> element
    // the instructions for which matchups are CORRECT are stored in the <resprocessing> element, after <presentation> closes.

    for(var i = 0; i < maxAnswers; i++) {

        // captures each left and right matchups
        var leftMatch = String(questionArray[3 + (i * 2)]); // cast to string to prevent issues with answers as numbers/bools
        var rightMatch = String(questionArray[4 + (i * 2)]);

        if(leftMatch == "" || leftMatch == "null" || rightMatch == "" || rightMatch == "null") { // if there's no answer, break
            break;
        }

        if(leftMatch == 'true' || leftMatch == 'false') { 
            leftMatch = leftMatch.toUpperCase(); // done to prevent string cast of bool from being lowercase, for looks only
        }

        if(rightMatch == 'true' || rightMatch == 'false') { 
            rightMatch = rightMatch.toUpperCase(); // done to prevent string cast of bool from being lowercase, for looks only
        }

        
        var responselidElement = XmlService.createElement('response_lid', namespace);
        responselidElement.setAttribute('ident', "Left" + (i + 1)); // gives ID to left value
        responselidElement.addContent(setMattext(leftMatch));

        var renderchoiceElement = XmlService.createElement('render_choice', namespace);
        if(shuffle) {
            renderchoiceElement.setAttribute("shuffle", "yes");
        }

        var responselabelElement = XmlService.createElement('response_label', namespace);
        responselabelElement.setAttribute('ident', "Right" + (i + 1)); // gives ID to right value
        responselabelElement.addContent(setMattext(rightMatch));

        renderchoiceElement.addContent(responselabelElement);
        responselidElement.addContent(renderchoiceElement);
        presentationElement.addContent(responselidElement); // left and right matches pushed into presentation element


        // XML for correct answers starts here, and is pushed into array.
        var respconditionElement = XmlService.createElement('respcondition', namespace);
        var conditionvarElement = XmlService.createElement('conditionvar', namespace);
        var varequalElement = XmlService.createElement('varequal', namespace);

        // ID for left and right values are matched to each other
        varequalElement.setAttribute('respident', "Left" + (i + 1));
        varequalElement.setText('Right' + (i + 1));

        conditionvarElement.addContent(varequalElement);
        respconditionElement.addContent(conditionvarElement);

        var setvarElement = XmlService.createElement('setvar', namespace);
        setvarElement.setAttribute('varname', 'SCORE');
        setvarElement.setAttribute('action', 'Add');
        setvarElement.setText(25.00);

        respconditionElement.addContent(setvarElement);
        respconditionElements.push(respconditionElement);
    }

    itemElement.addContent(presentationElement);

    var resprocessingElement = XmlService.createElement('resprocessing', namespace);
    var outcomesElement = XmlService.createElement('outcomes', namespace);
    var decvarElement = XmlService.createElement('decvar', namespace);

    // sets metadata about scoring.
    // I don't know how necessary this is, but I want this to replicate the exported code as much as possible.
    decvarElement.setAttribute('maxvalue', String(respconditionElements.length * 25));
    decvarElement.setAttribute('minvalue', '0');
    decvarElement.setAttribute('varname', 'SCORE');
    decvarElement.setAttribute('vartype', 'Decimal');

    outcomesElement.addContent(decvarElement);
    resprocessingElement.addContent(outcomesElement);

    // load in answer key array into <resprocessing>
    for(var i = 0; i < respconditionElements.length; i++) {

        resprocessingElement.addContent(respconditionElements[i]);

    }

    itemElement.addContent(resprocessingElement);
    return(itemElement);

}

// this function is very similar to matching; once both pass, I will try to refactor them into one function, or two functions that use a helper function.
function makeMissingwordsQuestion(questionArray, shuffle) {

    var question = questionArray[2];
    var questionType = questionArray[1];

    var regex = /\[\[(\d*)\]\]/;
    var answerSpaces = []; // the actual question

    while(regex.test(question)) {

        answerSpaces.push(question.match(regex)[1]);
        var newString = "[" + question.match(regex)[1] + "]";

        question = question.replace(regex, newString);

    }

    var itemElement = XmlService.createElement('item', namespace);
    itemElement.addContent(setQuestionTypeMetaData(questionType));

    var presentationElement = XmlService.createElement('presentation', namespace);
    presentationElement.addContent(setMattext(question));

    var maxAnswers = 6; // set all possible answers, up to six.
    var answers = []; // actual bank of answers
    var answerGroups = []; // which group the corresponding answer belongs to
    var correctAnswers = []; // booleans for if the answer is the correct one

    // grab each answer, as well as the group and if it's the correct answer.
    for(var i = 0; i < maxAnswers; i++) {

        var answer = String(questionArray[3 + (i * 2)]); // cast to string to prevent issues with answers as numbers/bools
        var answerGroup = questionArray[4 + (i * 2)];

        if(answer == "" || answer == "null" || answerGroup == "" || answerGroup == null) { // if there's no answer, break
            break;
        }

        if(answer == 'true' || answer == 'false') { 
            answer = answer.toUpperCase(); // done to prevent string cast of bool from being lowercase, for looks only
        }

        // if this is the first answer in the spreadsheet, it's correct. Save the identity for later.
        if(answerGroups.indexOf(answerGroup) != -1) {
            correctAnswers.push(false);
        }

        else {
            correctAnswers.push(true);
        }

        answers.push(answer);
        answerGroups.push(answerGroup);

    }

    var respconditionElements = []; // stores the XML for each correct answer matchup.
    // this is needed because even though the left and right matches are set in the for loop in the <presentation> element
    // the instructions for which matchups are CORRECT are stored in the <resprocessing> element, after <presentation> closes.

    var answerIdents = []; // saves the identities of correct answers (i.e. q1_a1), used to set what the correct answers are.

    // loop through each space where an answer is needed
    for(var i = 0; i < answerSpaces.length; i++) {
        
        var responselidElement = XmlService.createElement('response_lid', namespace);
        responselidElement.setAttribute('ident', "response_" + answerSpaces[i]); // gives ID to left value
        responselidElement.addContent(setMattext(answerSpaces[i]));

        var renderchoiceElement = XmlService.createElement('render_choice', namespace);
        if(shuffle) {
            renderchoiceElement.setAttribute("shuffle", "yes");
        }

        // loop through potential answers
        for(var j = 0; j < answers.length; j++) {

            if(answerGroups[j] == answerSpaces[i]) {

                if(correctAnswers[j]) {
                    answerIdents.push("q" + i + "_a" + j);
                }

                var responselabelElement = XmlService.createElement('response_label', namespace);
                responselabelElement.setAttribute("ident", "q" + i + "_a" + j); // gives ID to right value
                responselabelElement.addContent(setMattext(answers[j]));

                renderchoiceElement.addContent(responselabelElement);

            }

        }

        responselidElement.addContent(renderchoiceElement);
        presentationElement.addContent(responselidElement); // left and right matches pushed into presentation element

        // XML for correct answers starts here, and is pushed into array.
        var respconditionElement = XmlService.createElement('respcondition', namespace);
        var conditionvarElement = XmlService.createElement('conditionvar', namespace);
        var varequalElement = XmlService.createElement('varequal', namespace);

        varequalElement.setAttribute('respident', "response_" + answerSpaces[i]);
        varequalElement.setText(answerIdents[i]); // this is where answerIdents comes in; this sets the true answer.

        conditionvarElement.addContent(varequalElement);
        respconditionElement.addContent(conditionvarElement);

        var setvarElement = XmlService.createElement('setvar', namespace);
        setvarElement.setAttribute('varname', 'SCORE');
        setvarElement.setAttribute('action', 'Add');
        setvarElement.setText(25.00);

        respconditionElement.addContent(setvarElement);
        respconditionElements.push(respconditionElement);

    }

    itemElement.addContent(presentationElement);

    var resprocessingElement = XmlService.createElement('resprocessing', namespace);
    var outcomesElement = XmlService.createElement('outcomes', namespace);
    var decvarElement = XmlService.createElement('decvar', namespace);

    // sets metadata about scoring.
    // I don't know how necessary this is, but I want this to replicate the exported code as much as possible.
    decvarElement.setAttribute('maxvalue', String(respconditionElements.length * 25));
    decvarElement.setAttribute('minvalue', '0');
    decvarElement.setAttribute('varname', 'SCORE');
    decvarElement.setAttribute('vartype', 'Decimal');

    outcomesElement.addContent(decvarElement);
    resprocessingElement.addContent(outcomesElement);

    // load in answer key array into <resprocessing>
    for(var i = 0; i < respconditionElements.length; i++) {
        resprocessingElement.addContent(respconditionElements[i]);
    }

    itemElement.addContent(resprocessingElement);

    return(itemElement);

}

// this function was writen based on old documentation given to us by a Canvas representative
// it can be found here: https://github.com/instructure/qti/blob/master/spec/fixtures/items_1.2/ordering.xml
// it is entirely possible that this function creates extraneous XML code that is not necessary for the creation of
// ordering questions.
function makeOrderingQuestion(questionArray, shuffle) {

	var question = questionArray[2];
    var questionType = questionArray[1];

    var itemElement = XmlService.createElement('item', namespace);

    var presentationElement = XmlService.createElement('presentation', namespace);
    presentationElement.addContent(setMattext(question));

    var responselidElement = XmlService.createElement('response_lid', namespace);
    responselidElement.setAttribute('ident', 'Ordering');
    responselidElement.setAttribute('rcardinality', 'Ordered');

    var renderchoiceElement = XmlService.createElement('render_choice', namespace);
    if(shuffle) {
        renderchoiceElement.setAttribute("shuffle", "yes");
    }
    
    var flowlabelElement = XmlService.createElement('flow_label', namespace);

    // these elements determine the correct order of the answers. In this case, it'll just be sequential because
    // QB-Squared ordering questions are submitted in the correct order. Canvas does the shuffling for us.
    // Even though they're placed outside of the presentation element, they're easier to set inside of the loop.
    var varequalElements = [];

    var maxAnswers = 6; // set all possible answers, up to six.

    for(var i = 0; i < maxAnswers; i++) {

        var answer = String(questionArray[3 + (i * 2)]); // cast to string to prevent issues with answers as numbers/bools

        if(answer == "" || answer == "null") { // if there's no answer, break
            break;
        }

        if(answer == 'true' || answer == 'false') { 
            answer = answer.toUpperCase(); // done to prevent string cast of bool from being lowercase, for looks only
        }

        var responselabelElement = XmlService.createElement('response_label', namespace);
        responselabelElement.setAttribute('ident', i);

        responselabelElement.addContent(setMattext(answer));
        flowlabelElement.addContent(responselabelElement);

        var varequalElement = XmlService.createElement('varequal', namespace);
        varequalElement.setAttribute('respident', 'Ordering');
        varequalElement.setText(i);

        varequalElements.push(varequalElement);

	}

	renderchoiceElement.addContent(flowlabelElement);
	responselidElement.addContent(renderchoiceElement);
	presentationElement.addContent(responselidElement);
	itemElement.addContent(presentationElement);

	var resprocessingElement = XmlService.createElement('resprocessing', namespace);
	var outcomesElement = XmlService.createElement('outcomes', namespace);
	var decvarElement = XmlService.createElement('decvar', namespace);

	decvarElement.setAttribute('varname', 'ORDERSCORE');
	decvarElement.setAttribute('vartype', "Integer");
	decvarElement.setAttribute('defaultval', '1');

	outcomesElement.addContent(decvarElement);
	resprocessingElement.addContent(outcomesElement);

	var respconditionElement = XmlService.createElement('respcondition', namespace);
	var conditionvarElement = XmlService.createElement('conditionvar', namespace);

	for (var i = 0; i < varequalElements.length; i++) {
		conditionvarElement.addContent(varequalElements[i]);
	}

	var setvarElement = XmlService.createElement('setvar', namespace);
	setvarElement.setAttribute('action', 'Add');
	setvarElement.setAttribute('varname', 'SCORE1');
	setvarElement.setText('5');

	respconditionElement.addContent(conditionvarElement);
	respconditionElement.addContent(setvarElement);
	resprocessingElement.addContent(respconditionElement);
	itemElement.addContent(resprocessingElement);

    return(itemElement);

}

// blobs are an object, and are an argument needed in order to zip files
function getBlobs(rootFolder, path) {
  var blobs = [];
  var files = rootFolder.getFiles();
  while (files.hasNext()) {
    var file = files.next().getBlob();
    file.setName(path+file.getName());
    blobs.push(file);
  }
  var folders = rootFolder.getFolders();
  while (folders.hasNext()) {
    var folder = folders.next();
    var fPath = path+folder.getName()+'/';
    blobs.push(Utilities.newBlob([]).setName(fPath)); //comment/uncomment this line to skip/include empty folders
    blobs = blobs.concat(getBlobs(folder, fPath));
  }
  return blobs;
}

function stripFeedback(answer) {
    var answerToUpper = answer.toUpperCase();
    //Strip feedback from non-supported question types
    if (answerToUpper.indexOf("[FEEDBACK]") != -1) {
        answer = answer.substring(0, answerToUpper.indexOf("[FEEDBACK]"));
    }
    return answer.trim();
}