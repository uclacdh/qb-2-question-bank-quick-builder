# (QB)² - Question Bank Quick Builder - a Google Sheets Add-on 

## Installation
You can install (QB)2 from the [G-Suite Marketplace](https://gsuite.google.com/marketplace/app/qb%C2%B2_question_bank_quick_builder/1057685094279). 
You can also install directly from the Google Sheet by going to **Add-Ons > Get add-ons**.

## Setup
You can set up the spreadsheet template by going to **Add-ons > (QB)² – Question Bank Quick Builder > Set up (QB)2 spreadsheet**.

Alternatively, you can make a copy our (QB)2 templates. To save a copy of the file in your Google Drive, choose: **File > Make a copy**.

* [Spreadsheet Template – Samples](https://docs.google.com/spreadsheets/d/1AVX1bMdcI0LZK0FqjQhEaliLSwhEuwVJpO8OFmptojs/edit?usp=sharing)
* [Spreadsheet Template – Blank](https://docs.google.com/spreadsheets/d/1S7u1t1MFrOMKynHX1ATMpESm0mL4bOAfLJQhq03mES0/edit?usp=sharing)

## Documentation
* [User Documentation](https://humtech.atlassian.net/l/c/XDXkpzW0)
* [Offline Spreadsheet](https://humtech.atlassian.net/l/c/U9hPrneX)

## Video Tutorials
Step by step video tutorials can be found [here](https://www.youtube.com/playlist?list=PLVUSQFnPeh8SFYMrFf6ZAPjQ8KBYJ91WV)

## Credits
##### Developed By:
* Lillian Hawasli, Instructional Programmer
* Thomas Garbelotti, Humanities Instructional Technology Manager
* Benjamin Niedzielski, Research & Instructional Technology Consultant
* Amy Zhao, Student Programmer
* Jakin Wang, Student Programmer
* Rockford Mankini, Student Programmer

##### Logo/Art By:
* Robert Farley, Research & Instructional Technology Consultant

##### Testing/Support:
* Instructional Technology Group at Humanities Technology (HumTech), UCLA


## Current Version
* **Release:** Version 5.1.2
* **Released on:** 5/14/2021


Copyright (c) 2021 The Regents of the University of California. 
