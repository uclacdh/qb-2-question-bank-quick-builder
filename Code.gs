/*
(QB)² - Question Bank Quick Builder

The Clear BSD License

Copyright (c) 2021 Regents of the University of California
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted (subject to the limitations in the disclaimer
below) provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

     * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     * Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

var defaultAlignment = 'left';

var white = 'white';
var grey = '#D9D9D9', gray = '#D9D9D9';
var blue = '#0b5394';
var red ='#cc0000';
var yellow = '#ffe599';

var XML_ROOT_DEFAULT = "quiz";
var replaceExistingFiles = true;

var supportedQuestions = ['multichoice', 'shortanswer', 'truefalse', 'ordering', 'poodllrecording', 'essay', 'matching', 'cloze', 'dragdrop', 'missingwords', 'description', 'multianswer'];

function getSheetNames() {
    var spreadsheet = SpreadsheetApp.getActive();
    var sheets = spreadsheet.getSheets();
    var sheetNames = new Array();

    for (var i = 0; i < sheets.length; i++) {
        sheetNames.push(sheets[i].getName());
    }

    return sheetNames;
}

function getActiveSheetName() {
    var spreadsheet = SpreadsheetApp.getActive();

    return spreadsheet.getActiveSheet().getName();
}

function showCompilingMessage(title, opt_message, opt_width) {

    //optional params
    if (opt_message == null) {
        opt_message = '';
    }
    if (opt_width == null) {
        opt_width = 300;
        height = 110;
    } else {
        height = 120;
    }

    //Need to use createTemplateFromFile to call evaluate in order to parse js and css files
    var html = HtmlService.createTemplateFromFile('Spinner');
    html.message = opt_message;

    var htmlbody = html.evaluate().setWidth(opt_width).setHeight(height);

    SpreadsheetApp.getUi().showModalDialog(htmlbody, title);
}

function performExport() {
    exportMoodle(XML_ROOT_DEFAULT, replaceExistingFiles, getActiveSheetName());
}

function performCanvasExport() {
    exportCanvas(XML_ROOT_DEFAULT, replaceExistingFiles, getActiveSheetName());
}

function openQuestionSidebar() {
    validateQuestionTypes();
    var html = HtmlService
        .createTemplateFromFile("QuestionSidebar")
        .evaluate();
        html.setTitle('Add Questions');
        html.setWidth(300);
    SpreadsheetApp.getUi().showSidebar(html);
}

function performImport() {
    importMoodleWithPicker(getActiveSheetName(), "quiz");
}

function performBackupImport() {
    importMoodleWithPicker(getActiveSheetName(), "backup");
}


function openOptionSidebar() {
    //Need to use createTemplateFromFile to call evaluate in order to parse js and css files
    var html = HtmlService.createTemplateFromFile('OptionSidebar').evaluate()
        .setTitle('Options')
        .setWidth(300);
    SpreadsheetApp.getUi().showSidebar(html);
}

function getSetupLinks() {
    //Need to use createTemplateFromFile to call evaluate in order to parse js and css files
    var html = HtmlService.createTemplateFromFile('Setup').evaluate().setHeight(240);
    SpreadsheetApp.getUi().showModalDialog(html, 'Documentation & Templates');
}

function openAbout() {
    //Need to use createTemplateFromFile to call evaluate in order to parse js and css files
    var html = HtmlService.createTemplateFromFile('About').evaluate().setHeight(240);
    SpreadsheetApp.getUi().showModalDialog(html, 'About (QB)²');
}

//chose to use an alert with a button set to implement reset functionality
//since CSS on a template modal dialog would be consistent
function resetWarning() {
    var ui = SpreadsheetApp.getUi();
    var result = ui.alert(
        'Please confirm',
        'Resetting the spreadsheet will delete all questions. Are you sure you want to reset?',
        ui.ButtonSet.YES_NO);

    // Process the user's response.
    if (result == ui.Button.YES) {
        // User clicked "Yes".
        resetSpreadsheet();
    }
}

//Use an alert with a button set to implement question sidebar reset functionality
function sidebarResetWarning() {
    var ui = SpreadsheetApp.getUi();
    var result = ui.alert(
        'Please confirm',
        'Resetting this sidebar will delete your question and answers.\nAre you sure you want to reset?',
        ui.ButtonSet.YES_NO);

    // Process the user's response.
    return result == ui.Button.YES;
}

function onInstall(e) {
    onOpen(e);
}

//removed resetWarning.html template since it turned out to be unnecessary
//and the CSS on personal vs. UCLA gmail accounts was inconsistent
function onOpen(e) {

    // If authorization mode is None, only create a simple menu
    if (e && e.authMode == ScriptApp.AuthMode.NONE) {
      //Menu Items
      setMenu(false);
    } else {
        // If authorization mode is appropriate, 
        // we can see if the question bank sheet exists and set the menu accordingly
        var activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
        var mainSheet = activeSpreadsheet.getSheetByName("Question Bank");
        setMenu(mainSheet);
    }

};

function setMenu(isSet) {
    var menu = SpreadsheetApp.getUi().createAddonMenu();
    menu.addItem("Set up (QB)² spreadsheet", "setUp");

    if (isSet) {
        menu.addItem("Add Questions (guided)", "openQuestionSidebar");
        menu.addSubMenu(SpreadsheetApp.getUi().createMenu("Export to...")
            .addItem("XML (Moodle)", "performExport")
            .addItem("QTI XML (Canvas) [BETA]", "performCanvasExport")
            //.addItem("Plaintext", "plainTextExport")
            .addItem("MS Word (.docx)", "docxExport")
            .addItem("PDF", "pdfExport"));
        menu.addSubMenu(SpreadsheetApp.getUi().createMenu("Import from...")
            .addItem("XML (Moodle)", "performImport")
            .addItem("Moodle Backup XML [BETA]", "performBackupImport"));
        menu.addItem("Options", "openOptionSidebar");
    }
    menu.addItem("Documentation & Templates", "getSetupLinks")
    menu.addItem("About (QB)²", "openAbout")
    menu.addToUi();
}

function onEdit(e) { //needs to be called onEdit for the fraction dropdown to work
    var sheet = SpreadsheetApp.getActiveSheet();
    var myRange = SpreadsheetApp.getActiveRange();
    var row = myRange.getRow();
    var val = myRange.getValue();
    var s_name = sheet.getName();

    if(s_name != "Categories" && s_name != "QuestionTypes" && s_name != "QuestionOptions" && s_name != "Import Errors") {

        if(myRange.getColumn() == 2 && row > 2) {
            prepareDataValidation(sheet, row, val, true);
        }
    } else if(s_name == "QuestionTypes" && row == 1) {

        var ui = SpreadsheetApp.getUi();

        // removing a supported question type
        if (supportedQuestions.indexOf(e.oldValue) != -1) {
            var result = ui.alert(
                "Supported Question Type Removed",
                "You just removed a question type from the spreadsheet. You should undo the change immediately, or import/export may not work as expected.",
                ui.ButtonSet.OK);
        }

        // adding an unsupported question type
        else if (supportedQuestions.indexOf(val) == -1 && val != ""){
            var result = ui.alert(
            "Unsupported Question Type Added",
            "You just added an unsupported question type to the spreadsheet. You should undo the change immediately, or import/export may not work as expected.",
            ui.ButtonSet.OK);
        }
    } 
}

function prepareDataValidation (sheet, row, q_type, overwriteValues) {
    //clear data validation first
    clearDataValidation(sheet, row);
    //set color of the cells for the row
    set_color_cells(sheet, row, q_type);
    //set data validation
    switch(q_type) {
        case "multichoice":
            setDataValidation(sheet, row, 1);
            break;
        case "shortanswer":
            setDataValidation(sheet, row, 2);
            break;
        case "truefalse":
            answerOptionsTF(sheet, row, overwriteValues);
            setDataValidation(sheet, row, 3, 2);
            break;
        case "dragdrop":
            setDataValidation(sheet, row, 9);
            break;
        case "missingwords":
            setDataValidation(sheet, row, 10);
            break;
        case "multianswer":
        	setDataValidation(sheet, row, 12);
    }
}

function setDataValidation (sheet, row, startCol, maxCol) {
    var dvSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("QuestionTypes");
    var option = dvSheet.getSheetValues(2,startCol,100,1);
    var dv = SpreadsheetApp.newDataValidation();
    dv.setAllowInvalid(true);
    dv.requireValueInList(option, true);

    for(i = 0; i < dropDownCols.length; i++) {

        //an exception in the case of T/F question type, where only two columns need data validation
        if(maxCol !== null && i >= maxCol) { break; }

        sheet.getRange(row, dropDownCols[i]).setDataValidation(dv.build());
    }
}

function clearDataValidation(sheet, row){
    dropDownCols.forEach(function(ddcol) {
        sheet.getRange(row,ddcol).clearDataValidations();
    });
}

function answerOptionsTF(sheet, row, overwriteValues){
    if (overwriteValues) {
        sheet.getRange(row, Math.min.apply(null, dropDownCols)-1, 1, Math.max.apply(null, dropDownCols) - Math.min.apply(null, dropDownCols) + 2).clearContent();
    }

    sheet.getRange(row, 4).setValue('TRUE').setHorizontalAlignment(defaultAlignment);
    sheet.getRange(row, 6).setValue('FALSE').setHorizontalAlignment(defaultAlignment);
}

//colors cells grey or white depending on the q_type (question type) parameter
function set_color_cells(sheet, row, q_type) {
    var colStart = Math.min.apply(null, dropDownCols);
    var colEnd = Math.max.apply(null, dropDownCols);
    switch (q_type) {
        case "truefalse":
            sheet.getRange(row, colStart + 3, 1, colEnd - colStart - 2).setBackground(grey);
            sheet.getRange(row, colStart - 1, 1, 4).setBackground(white);
            break;
        //Ordering questions do not need a score per answer, so they must be treated separately
        case "ordering":
            for (i = colStart - 1; i <= colEnd - 1; i = i + 2) {
                sheet.getRange(row, i, 1, 1).setBackground(white);
                sheet.getRange(row, i + 1, 1, 1).setBackground(grey);
            }
            break;
        //If user selects an question type with no answers needed, we grey out answer choice cells
        case "essay":
        case "poodllrecording":
        case "cloze":
        case "description":
            sheet.getRange(row, colStart - 1, 1, colEnd - colStart + 2).setBackground(grey);
            break;
        //If user selects a question type where all answer cells may be filled, we make all such cells white
        //At present, this is also used for undefined question types
        default:
            sheet.getRange(row, colStart - 1, 1, colEnd - colStart + 2).setBackground(white);
    }
}

function resetSpreadsheet() {
    var sheet = SpreadsheetApp.getActiveSheet();
    var sheetName = sheet.getName();
    var range = sheet.getDataRange();
    var numRows = range.getNumRows();
    if(numRows == 0 | numRows == 1 | numRows == 2) return;
    var numColumns = range.getNumColumns();
    if (sheetName != "Categories" && sheetName != "QuestionTypes" && sheetName != "QuestionOptions" && sheetName != "Import Errors") {
        sheet.getRange(3, 1, numRows - 2, numColumns).setBackground(white);
        sheet.getRange(3, 1, numRows - 2, numColumns).clearContent();
        sheet.getRange(3, 3, numRows - 2, numColumns - 2).clearDataValidations();
    }
}

//Return the list of categories
//Due to how getValues() works, the result is a 2D array with [0][0] displaying "Categories"
function getCategoryList() {
    var categorySheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Categories");
    if (categorySheet == null) {
        return null;
    }
    return categorySheet.getDataRange().getDisplayValues();
}

// Add a new Category the user inputted to the sheet and give a notification of success or failure.
function AddToSheet(newCategory){
    var categorySheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Categories");
    if (categorySheet == null) {
        return null;
    }

    var categories = categorySheet.getDataRange().getDisplayValues().map(function(subarr){return subarr[0];});
    var ui = SpreadsheetApp.getUi();

    var check = categories.indexOf(newCategory);
    if(check != -1){
        // Don't allow users to add duplicate categories
        ui.alert(
            'Duplicate',
            'This category already exists', 
            ui.ButtonSet.OK);
        return null;
    }else{
        var nextrow = categories.length + 1; 
        categorySheet.getRange(nextrow, 1).setValue(newCategory);
        return newCategory;
    }
}

//Return the list of supported questions
function getSupportedQuestions() {
    return supportedQuestions;
}

//Returns the list of scores and groups in the QuestionOptions sheet
//Due to how getValues() works, the result is a 2D array with [0][0] displaying "Point Percentage"
//and [0][1] displaying "Group"
function getScoreAndGroupChoices() {
    var optionsSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("QuestionTypes");
    if (optionsSheet == null) {
        return null;
    }
    return optionsSheet.getDataRange().getDisplayValues();
}

//Returns the number of answer columns currently supported by QB^2
function getNumOfAnswers() {
    var colStart = Math.min.apply(null, dropDownCols);
    var colEnd = Math.max.apply(null, dropDownCols);
    return (colEnd - colStart)/2 + 1;
}

//Adds a question from the sidebar to the spreadsheet
//Returns the line number of the added question, or -1 if the question couldn't be added.
function addSidebarQuestion(questionType, questionArray, hasMedia) {
    try {
        if (getSupportedQuestions().indexOf(questionType) == -1) {
            //unsupported
            return -1;
        }
        var sheet = SpreadsheetApp.getActive().getActiveSheet();
        var activeSheetName = sheet.getName();
        //prevent write to non-QB sheet
        if (activeSheetName == 'Categories' || activeSheetName == 'QuestionTypes' || activeSheetName == 'QuestionOptions' || activeSheetName == 'Import Errors') {
            var ui = SpreadsheetApp.getUi();
            var result = ui.alert(
                'Switch to Question Bank',
                'Questions are added to the sheet that is currently visible.\nPlease navigate to your question bank and try again.',
                ui.ButtonSet.OK);
            return -1;
        }

        var row = sheet.getDataRange().getNumRows();
        if (String(sheet.getRange(row, 2).getValue()) != "" || String(sheet.getRange(row, 3).getValue()) != "") {
            //If there are questions, this returns the final row with questions
            //So we need to increment by 1
            row++;
        }

        //Leave 10 blank rows after the question
        if (row >= sheet.getMaxRows()) {
            sheet.insertRowsAfter(row - 1, 11);
        }

        //write the question
        //set validation and grey out unused cells
        try {
            prepareDataValidation(sheet, row, questionArray[1], true);
        } catch(e) {
            //Validation failed, likely because the user changed the category
            var ui = SpreadsheetApp.getUi();
            var result = ui.alert(
                'Check Category',
                'It appears that the category you have selected has been renamed or deleted. Click the refresh button and try choosing the category again.',
                ui.ButtonSet.OK);
            return -1;
        }

        var cells = [];

        for (var col = 0; col < questionArray.length; col++) {
            var cell = questionArray[col];
            if (cell != null) {
                //add media tag to the question if needed
                if (col == 2 && hasMedia) {
                    cell = "[MEDIA] " + cell;
                }
                    cells.push(cell);
            }
        }

        var inputCells = [cells];
        sheet.getRange(row, 1, 1, questionArray.length).setValues(inputCells).setHorizontalAlignment(defaultAlignment);

        return row;
    } catch(e) {
        //Unknown error
        return -1;
    }
}

// Checks if question types in the sheet matches the array. If not, attempt to reset spreadsheet.
function validateQuestionTypes() {
    var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("QuestionTypes");
    var range = sheet.getRange(1, 1, 1, supportedQuestions.length);
    vals = range.getValues()[0];

    var isEqual = true;
    for(var i = 0; i < vals.length; ++i) {
        if (vals[i] !== supportedQuestions[i]) {
            isEqual = false;
        }
    }

    if(isEqual == false) {
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
        "Question Types Not Valid",
        "There appears to be an issue with the question types internally set in the spreadsheet. This could be from adding or removing certain question types. \n\n Press OK to reset the spreadsheet (custom point values will be lost, but everything else, including your questions, will be maintained). If you do not reset the spreadsheet, we cannot guarantee Import or Adding Questions (guided) will work as expected.",
        ui.ButtonSet.OK_CANCEL);
        if (result == ui.Button.OK) {
            setUp(true);
        }
    }
}

// Returns an object representing the current settings for each setting in the
// defaultSettings object.  If the setting is not set, creates the property
// with the specified default value.
// defaultSettings will resemble {"optionCheckbox" : true}
function loadProperties(defaultSettings) {
    var toReturn = {};
    var settings = PropertiesService.getDocumentProperties().getProperties();
    for (var property in defaultSettings) {
        if (property in settings) {
            toReturn[property] = settings[property];
        } else {
            toReturn[property] = defaultSettings[property];
            saveProperty(property, defaultSettings[property]);
        }
    }

    return toReturn
}

// Changes a setting in DocumentProperties
function saveProperty(propName, propValue) {
    PropertiesService.getDocumentProperties().setProperty(propName, propValue);
}

// For including CSS and JavaScript files
// Builds a template HTML file.
// Filename is the name of the HTML file to include, without the .html suffix
function include(filename) {
    try {
        return HtmlService.createHtmlOutputFromFile(filename)
        .getContent();
    } catch (err) {
        Logger.log("Could not find a file called " + filename + ".html");
        return "";
    }
}
