/*
(QB)² - Question Bank Quick Builder

The Clear BSD License

Copyright (c) 2021 Regents of the University of California
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted (subject to the limitations in the disclaimer
below) provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

     * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     * Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

function setUp(isUpdate) {

    if(isUpdate == null) {
        isUpdate = false;
    }

    if (isUpdate || !checkExisting()) {
        showCompilingMessage("Setting up (QB)² spreadsheet", "This may take a few minutes...", 350);
        createCategories();
        createQuestionTypes();
        createMainSheet();

        // After Main Sheet is created, all Options have to be added back
        setMenu(true);

        orderSheets();
        setMenu(true);
        var ui = SpreadsheetApp.getUi()
        ui.alert("Welcome to (QB)²", "Your spreadsheet is ready for use.", ui.ButtonSet.OK);
        if(!isUpdate) {
            openQuestionSidebar();
        }
    }
}

function orderSheets() {
    var activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var mainSheet = activeSpreadsheet.getSheetByName("Question Bank");
    SpreadsheetApp.setActiveSheet(mainSheet);
    activeSpreadsheet.moveActiveSheet(1);
}

function checkExisting() {
    var activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var typeSheet = activeSpreadsheet.getSheetByName("QuestionTypes");
    var categorySheet = activeSpreadsheet.getSheetByName("Categories");
    var mainSheet = activeSpreadsheet.getSheetByName("Question Bank");
    if (typeSheet != null || categorySheet != null || mainSheet != null) {
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
            "Reset Warning",
            "It appears that this spreadsheet is already set up as a (QB)² spreadsheet. " +
            "This option will overwrite the headers and necessary columns of the existing sheets. " +
            "Any existing questions below row 2 on the \"Question Bank\" sheet and below row 1 in the \"Categories\" sheet will not be erased.\n\n" +
            "Are you sure you want to continue setting up a (QB)² spreadsheet?",
            ui.ButtonSet.OK_CANCEL);
        if (result != ui.Button.OK)
            return true;
    }
    return false;
}

function createCategories() {

    var activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var yourNewSheet = activeSpreadsheet.getSheetByName("Categories");

    if (yourNewSheet == null) {
        yourNewSheet = activeSpreadsheet.insertSheet();
    }

    yourNewSheet.setName("Categories");
    yourNewSheet.setColumnWidth(1, 500).setRowHeight(1, 60);

    var headerStyle = SpreadsheetApp.newTextStyle()
        .setForegroundColor(white)
        .setFontSize(11)
        .setBold(true).build();

    yourNewSheet.getRange("A1")
        .setDataValidation(null)
        .clear()
        .setValue("Categories")
        .setTextStyle(headerStyle)
        .setBackground(blue)
        .setHorizontalAlignment('center')
        .setVerticalAlignment('middle');

    //Deleting extra empty columns
    var firstCol = 2;
    var colsToDelete = yourNewSheet.getMaxColumns() - firstCol + 1;
    if (colsToDelete >= 1) {
        yourNewSheet.deleteColumns(firstCol, colsToDelete);
    }
}

function createQuestionTypes() {

    var activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var yourNewSheet = activeSpreadsheet.getSheetByName("QuestionTypes");

    if (yourNewSheet == null) {
        yourNewSheet = activeSpreadsheet.insertSheet();
    }

    yourNewSheet.setName("QuestionTypes");
    if (yourNewSheet.getMaxColumns() < 11) yourNewSheet.insertColumnsAfter(yourNewSheet.getMaxColumns(), 11 - yourNewSheet.getMaxColumns());
    var categories = [
        ["multichoice",
            "shortanswer",
            "truefalse",
            "ordering",
            "poodllrecording",
            "essay",
            "matching",
            "cloze",
            "dragdrop",
            "missingwords",
            "description",
            "multianswer"
        ]
    ];

    var pointPercentageScore = [
        [0],
        [25],
        [50],
        [100]
    ];

    var pointPercentageScoreNegative = [
        [-25],
        [-50],
        [-100]
    ];

    var groupScore = [
        [1],
        [2],
        [3],
        [4],
        [5],
        [6],
        [7],
        [8],
        [9],
        [10]
    ];

    var range = yourNewSheet.getRange("A1:L1");
    range.setDataValidation(null).clear().setValues(categories);
    yourNewSheet.getRange("A2:A5").setDataValidation(null).clear().setValues(pointPercentageScore);
    yourNewSheet.getRange("B2:B5").setDataValidation(null).clear().setValues(pointPercentageScore);
    yourNewSheet.getRange("C2:C5").setDataValidation(null).clear().setValues(pointPercentageScore);
    yourNewSheet.getRange("I2:I11").setDataValidation(null).clear().setValues(groupScore);
    yourNewSheet.getRange("J2:J11").setDataValidation(null).clear().setValues(groupScore);
    yourNewSheet.getRange("L2:L5").setDataValidation(null).clear().setValues(pointPercentageScore);
    yourNewSheet.getRange("L6:L8").setDataValidation(null).clear().setValues(pointPercentageScoreNegative);
    yourNewSheet.hideSheet();

}

function createMainSheet() {
    var activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    var yourNewSheet = activeSpreadsheet.getSheetByName("Question Bank");

    if (yourNewSheet == null) {
        yourNewSheet = activeSpreadsheet.insertSheet();
    }

    var whiteText = SpreadsheetApp.newTextStyle().setForegroundColor(white).build();

    yourNewSheet.setName("Question Bank");
    if (yourNewSheet.getMaxColumns() < 15) yourNewSheet.insertColumnsAfter(yourNewSheet.getMaxColumns(), 15 - yourNewSheet.getMaxColumns());
    yourNewSheet.getRange("A1:O2").setDataValidation(null).clear().setBackground(blue);

    yourNewSheet.getRange("B1:C1").setVerticalAlignment('top');
    yourNewSheet.getRange("B1").setValue("Notes:").setTextStyle(whiteText).setHorizontalAlignment('right');
    yourNewSheet.getRange("C1").setValue("This is some space for the instructor to be able to write any additional notes he or she would like without interfering with the plugin")
        .setTextStyle(SpreadsheetApp.newTextStyle().setForegroundColor(yellow).build()).setWrap(true);

    var headerStyle = SpreadsheetApp.newTextStyle()
        .setForegroundColor(white)
        .setFontSize(11)
        .setBold(true).build();
    yourNewSheet.getRange("A2:O2").setTextStyle(headerStyle)
        .setBackgroundColor(blue)
        .setHorizontalAlignment('center')
        .setVerticalAlignment('middle');


    yourNewSheet.getRange("A2:C2").setValues([
        ["Category", "Type", "Question/Description"]
    ]);

    var text = [
        ["Answer",
            "Value /\nGroup /\nAnswer"
        ]
    ];
    var answer_column = 4,
        answer_end = 14;

    for (var i = answer_column; i <= answer_end; i = i + 2) {
        yourNewSheet.getRange(2, i, 1, 2).setValues(text);
        yourNewSheet.setColumnWidth(i, 150);
        yourNewSheet.setColumnWidth(i + 1, 70);
    }

    yourNewSheet.setRowHeights(1, 2, 60);
    yourNewSheet.setColumnWidth(1, 150);
    yourNewSheet.setColumnWidth(2, 150);
    yourNewSheet.setColumnWidth(3, 500);
    yourNewSheet.setFrozenRows(2);

    var categories = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Categories").getRange("A2:A");
    var category_rule = SpreadsheetApp.newDataValidation().setAllowInvalid(false).requireValueInRange(categories).build();
    yourNewSheet.getRange("A3:A").setDataValidation(category_rule);
    var question_types = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("QuestionTypes").getRange("1:1")
    var question_type_rule = SpreadsheetApp.newDataValidation().setAllowInvalid(false).requireValueInRange(question_types).build()
    yourNewSheet.getRange("B3:B").setDataValidation(question_type_rule);

    //Deleting extra empty columns
    var firstCol = 16;
    var colsToDelete = yourNewSheet.getMaxColumns() - firstCol + 1;
    if (colsToDelete >= 1) {
        yourNewSheet.deleteColumns(firstCol, colsToDelete);
    }
}