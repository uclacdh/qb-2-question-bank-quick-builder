/*
(QB)² - Question Bank Quick Builder

The Clear BSD License

Copyright (c) 2021 Regents of the University of California
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted (subject to the limitations in the disclaimer
below) provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

     * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     * Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

//Alerts are used for many popups but do not allow HTML.  These constants replicate the
//OK button in alerts for an HTML window instead.
var okButtonStyling = 'background-image: none; border-radius: 4px; box-shadow: none;' +
                    'box-sizing: border-box; font-family: \'Google Sans\',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;' +
                    'font-weight: 500; font-size: 14px; height: 36px; letter-spacing: 0.25px;' +
                    'line-height: 16px; padding: 9px 24px 11px 24px; background: white;' +
                    'border: 1px solid #dadce0!important; color: #188038; margin: 0 0 0 12px;' +
                    'cursor: pointer; min-width: 72px; outline: 0;';
var okButtonHTML = '<p><div style="display: flex; justify-content: flex-end; margin-top: 24px;">' +
                    '<button style="' + okButtonStyling + '" onclick="google.script.host.close()" name="1">Ok</button></div></p>'

//List for collecting error text during imports
var importErrors = [];
// For Image Tag importing and exporting
var allowedAttrs = ["src", "alt", "width", "height", "class"];
// For Image Tag Importing. To differentiate PlugIn and SRC.
var isPlugin = false;

/****************************************************
 * The following functions handle the file picker
 ****************************************************/

//Save important settings in the cache and display the file or folder picker
function importMoodleWithPicker(activeSheetName, importType) {
    var properties = PropertiesService.getDocumentProperties();
    var settings = properties.getProperties();
    var includeHTML = false;
    var importFolder = false;
    var clozeOption = false;

    if ('htmlCheckbox' in settings) {
        includeHTML = (settings['htmlCheckbox'] == "true");
    } else {
        properties.setProperty('htmlCheckbox', false);
    }

    if ('folderCheckbox' in settings) {
        importFolder = (settings['folderCheckbox'] == "true");
    } else {
        properties.setProperty('folderCheckbox', false);
    }

    if ('clozeCheckbox' in settings) {
        clozeOption = (settings['clozeCheckbox'] == "true");
    } else {
        properties.setProperty('clozeCheckbox', false);
    }

    var scriptProperties = PropertiesService.getScriptProperties();

    if(importType == "backup") {
        scriptProperties.setProperty("backup", true);

        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
                'Import Moodle Backup',
                'This is for importing a Moodle Backup file (an exported XML file of the entire course) as a document to summarise course content. By default, this file is named moodle_backup.xml. Importing non-Moodle Backup files may result in unexpected behavior.',
                ui.ButtonSet.OK);
    }

    else {
        scriptProperties.setProperty("backup", false);
    }

    //We import to the active sheet, but this is undesirable if the active sheet is not a question bank
    //Let the user know they need to change sheets if they are on one of the following
    if (activeSheetName == 'Categories' || activeSheetName == 'QuestionTypes' || activeSheetName == 'QuestionOptions' || activeSheetName == 'Import Errors') {
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
                'Switch to Question Bank',
                'Import adds questions to the sheet that is currently visible.\nPlease navigate to your question bank and try again.',
                ui.ButtonSet.OK);
        return;
    }

    // Gets a persistent associative array that is specific to the current
    // document containing the script
    var properties = PropertiesService.getDocumentProperties();
    var propertyVals = properties.getProperties();
    if ("importInProgress" in propertyVals && propertyVals["importInProgress"]) {
        //Import already in progress.
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
            'Error',
            'Please wait for your current import to complete before starting a new one.',
            ui.ButtonSet.OK);
        //Do not call deleteImportProperties, since another import is ongoing.
        return;
    }
    var newProperties = {"activeSheetName": activeSheetName,
                         "includeHTML": includeHTML,
                         "clozeOption": clozeOption};
    properties.setProperties(newProperties);

    var fileName = 'FilePicker';
    var modalDialogTitle = 'Select the Moodle XML file to import!';
    if (importFolder) {
        fileName = 'FolderPicker';
        modalDialogTitle = 'Select the folder with Moodle XML files to import!';
    }
    var html = HtmlService.createTemplateFromFile(fileName)
            .evaluate()
            .setWidth(600)
            .setHeight(425)
            .setSandboxMode(HtmlService.SandboxMode.IFRAME);
    SpreadsheetApp.getUi().showModalDialog(html, modalDialogTitle);
}

//Generates an OAuth token for permission to use the Google Picker
function getOAuthToken() {
    DriveApp.getRootFolder();
    return ScriptApp.getOAuthToken();
}

/****************************************************
 * The following functions choose the files to import
 ****************************************************/

//Import the file(s) specified by the Google id.
//importFolder is true if id represents a folder
function importMoodle(id, importFolder) {

    // check if the user is importing a moodle backup file. if not, proceed with regular import
    var scriptProperties = PropertiesService.getScriptProperties();

    // the property gets coerced to a string.
    if(scriptProperties.getProperty("backup") == "true") {

        try {

            //Restore variables from the properties associative array
            //Necessary to ensure that settings are stored correctly
            var properties = PropertiesService.getDocumentProperties();
            var propertyVals = properties.getProperties();
            properties.setProperty("importInProgress", true);


            includeHTML = (includeHTML == "true");

            importErrors = [];
            //Get and clear the Import Errors sheet, if it exists
            //We do not create a new sheet if it doesn't since the
            //import may be successful
            clearErrorSheet();

            var filesToImport = getFileList(id, importFolder);
            var filesRead = 0;
            if (filesToImport.length != 0) {
                for (var i = 0; i < filesToImport.length; i++) {
                    var file = filesToImport[i];
                    if (file != null) {
                        showCompilingMessage('Reading Moodle XML file ...', 'Currently reading ' + file.getName() + '. This may take awhile ...', 400);
                        var success = importDocumentMoodleBackup(file, activeSheetName, includeHTML);
                        if (success) {
                            filesRead++;
                        }
                    }
                }
            }

        }

        catch(e) {

            var ui = SpreadsheetApp.getUi();
            var result = ui.alert(
                'Moodle Backup Import Failed',
                'An error occurred while importing that prevented the import process from completing. ' +
                'Please ensure that your files are properly formatted Moodle XML files.',
                ui.ButtonSet.OK);


        }

    }

    else {

        try {
            //Restore variables from the properties associative array
            //Necessary to ensure that settings are stored correctly
            var properties = PropertiesService.getDocumentProperties();
            var propertyVals = properties.getProperties();
            properties.setProperty("importInProgress", true);

            var activeSheetName = propertyVals["activeSheetName"];
            var includeHTML = propertyVals["includeHTML"];
            if (activeSheetName == null || includeHTML == null) {
                //Properties have been cleared unexpectedly.  This shouldn't happen but
                //we need to check just in case
                var ui = SpreadsheetApp.getUi();
                var result = ui.alert(
                    'Error',
                    'Something has gone wrong - please try your import again.',
                    ui.ButtonSet.OK);
                deleteImportProperties();
                return;
            }
            includeHTML = (includeHTML == "true");

            importErrors = [];
            //Get and clear the Import Errors sheet, if it exists
            //We do not create a new sheet if it doesn't since the
            //import may be successful
            clearErrorSheet();

            var filesToImport = getFileList(id, importFolder);
            var filesRead = 0;
            if (filesToImport.length != 0) {
                for (var i = 0; i < filesToImport.length; i++) {
                    var file = filesToImport[i];
                    if (file != null) {
                        showCompilingMessage('Reading Moodle XML file ...', 'Currently reading ' + file.getName() + '. This may take awhile ...', 400);
                        var success = importSpreadsheetMoodle(file, activeSheetName, includeHTML);
                        if (success) {
                            filesRead++;
                        }
                    }
                }
            }
            showImportCompleteMessage(filesRead);

        } catch(e) {
            var ui = SpreadsheetApp.getUi();
            var result = ui.alert(
                'Moodle Import Failed',
                'An error occurred while importing that prevented the import process from completing. ' +
                'Please ensure that your files are properly formatted Moodle XML files.',
                ui.ButtonSet.OK);
        }
    }
    deleteImportProperties();
}

//Returns a list of XML files of proper size based on the Google id
//importFolder is true if id refers to a folder not a file
function getFileList(id, importFolder) {
    var filesToImport = [];
    if (!importFolder) {
        var file = DriveApp.getFileById(id);
        if (file != null) {
            filesToImport.push(file);
        }
    } else {
        //Get folder given by this ID number
        var folder = DriveApp.getFolderById(id);
        if (folder != null) {
            var files = folder.getFiles();
            while(files.hasNext()) {
                var file = files.next();
                filesToImport.push(file);
            }
        }
    }

    //Remove obviously invalid files
    for (var i = 0; i < filesToImport.length; i++) {
        var file = filesToImport[i];
        if (file.getName().indexOf(".xml") == -1) {
            //remove non-XML files
            filesToImport.splice(i, 1);
            //Decrease index because we removed from the list
            i--;
            continue;
        }
        if (file.getSize() > 50000000) {
            //Google cannot parse files over 50 MB
            filesToImport.splice(i, 1);
            i--;
            importFileFailed(file, 'File size exceeds 50 MB, so Google cannot read it.');
            continue;
        }
    }

    return filesToImport;
}

/****************************************************
 * The following functions are helper functions
 * to prepare for the import process
 ****************************************************/

//Get and clear the error sheet if it exists
function clearErrorSheet() {
    var activeSpreadsheet = SpreadsheetApp.getActive();

    var errorSheet = activeSpreadsheet.getSheetByName("Import Errors");
    if (errorSheet != null) {
        errorSheet.clear();
        //Set the first row to be a bold message alerting the user to errors
        errorSheet.getRange(1, 1).setValue("Errors found during import:")
                .setTextStyle(SpreadsheetApp.newTextStyle().setBold(true).build()).setHorizontalAlignment(defaultAlignment);
    }
}

//Returns all categories currently in the active spreadsheet, as a dictionary to remove duplicates
//The category name is the key. The value is true for categories already in the spreadsheet to distinguish
//from those added through import.
function getExistingCategories(spreadsheet) {
    var categorySheet = spreadsheet.getSheetByName("Categories");
    var categories = categorySheet.getDataRange().getValues();
    var categoriesDict = {}
    for (var i = 0; i < categories.length; i++) {
        categoriesDict[categories[i][0]] = true;
    }
    return categoriesDict;
}

//Removes import information stored in PropertiesService to signal the completion
//of the import.
function deleteImportProperties() {
    var properties = PropertiesService.getDocumentProperties();
    properties.deleteProperty("importInProgress");
}

//Returns all Question Types currently in the active spreadsheet, as a dictionary to remove duplicates
//The question type name is the key. The value is true for categories already in the spreadsheet to distinguish
//from those added through import.
function getExistingQuestionTypes(spreadsheet) {
    var questionTypeSheet = spreadsheet.getSheetByName("QuestionTypes");
    var lastColumn = questionTypeSheet.getLastColumn();
    var questionTypes = questionTypeSheet.getRange(1, 1, 1, lastColumn).getValues();
    var questionTypesArray = []
    for (var i = 0; i < questionTypes[0].length; i++) {
        if(questionTypes[0][i].length > 0)
            questionTypesArray.push(questionTypes[0][i]);
    }
    return questionTypesArray;
}



/****************************************************
 * The following function parses a single file
 * for import
 ****************************************************/

//Import a single spreadsheet into QB2
function importSpreadsheetMoodle(file, activeSheetName, includeHTML) {
    var spreadsheet = SpreadsheetApp.getActive();
    var sheet = spreadsheet.getSheetByName(activeSheetName);
    var fileName = file.getName();

    try {
        //Read the XML file as a string
        //Ensure the file can be parsed as text
        var importContent = "";
        var document;
        try {
            //Moodle and QB2 use different quotation marks
            importContent = file.getBlob().getDataAsString("UTF-8").trim();
            document = XmlService.parse(importContent);
        } catch(e) {
            importFileFailed(fileName, "Google was unable to read the file as valid XML. It returned the following error message.\n" + e);
            return false;
        }

        var questions = document.getRootElement().getChildren('question');
        if (questions == null || questions.length == 0) {
            return false;
        }

        var categories = getExistingCategories(spreadsheet);
        var questionTypes = getExistingQuestionTypes(spreadsheet);

        var questionArr = [];
        var questionNumber = 0;
        var currentCategory = "";
        for (var i = 0; i < questions.length; i++) {
            //Parse each question
            var question = questions[i];
            var questionType = getQuestionType(question);

            if (questionType == null) {
                continue;
            } else if (questionType == 'category') {
                currentCategory = parseCategory(fileName, question, sheet, includeHTML, categories);
            } else {
                // If the question Type on the QuestionTypes Sheet is not found, append the error to the Error Import Sheet.
                if (questionTypes.indexOf(questionType) == -1) {
                    if(supportedQuestions.indexOf(questionType) == -1){
                        importQuestionFailed(fileName, parseQuestionName(question), "Could not import question \'" + questionType + "\' is not yet supported.", false);
                    }else{
                        importQuestionFailed(fileName, parseQuestionName(question), "There appears to be an issue with the question types internally set in the spreadsheet (" + questionType + "). This could be from adding or removing certain question types. Please reset the spreadsheet to fix this issue by going to Add-ons > QB2 > Set Up (QB)2 Spreadsheet", false);
                    }
                }else{
                    var parseResults = parseQuestion(fileName, question, questionType, sheet, includeHTML, categories);
                    if (parseResults != null) {
                        parseResults[0] = currentCategory;
                        questionArr.push(parseResults.slice());
                        questionNumber++;
                    }
                    
                }
            }
        }
        //Write results
        writeCategories(sheet, categories);
        writeQuestions(sheet, questionArr);
    } catch (e) {
        importFileFailed(fileName, "We were unable to parse your file as Moodle XML.  Please make sure it has been exported correctly.");
        return false;
    }
    return true;
}

/****************************************************
 * The following functions parse individual
 * questions for import
 ****************************************************/

//Parses the question and returns an array of how the question will appear in the spreadsheet, or null if unsuccessful.
function parseQuestion(fileName, questionXML, questionType, sheet, includeHTML, categories) {
    if (questionXML == null) {
        return null;
    }

    switch(questionType) {
        case 'description':
        case 'essay':
        case 'poodllrecording':
            //We can treat these types together because they have no answers
            return parseNoAnswerQuestion(fileName, questionXML, questionType, sheet, includeHTML);
        case 'multichoice':
        case 'shortanswer':
        case 'truefalse':
        case 'matching':
        case 'dragdrop':
        case 'missingwords':
            return parseMultiQuestion(fileName, questionXML, questionType, sheet, true, includeHTML);
        case 'ordering':
            //Similar to above, but no scores involved
            return parseMultiQuestion(fileName, questionXML, questionType, sheet, false, includeHTML);
        case 'cloze':
            return parseClozeQuestion(fileName, questionXML, sheet, includeHTML);
        default:
            importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "The question type \'" + questionType + "\' is not yet supported.", false);
            break;
    }

    return null;
}

//Parse a question type that does not have answers
//Returns an array representing the results of that parse, or null if it fails
function parseNoAnswerQuestion(fileName, questionXML, questionType, sheet, includeHTML) {
    var parseResults = new Array(3);
    var questionText = parseQuestionText(questionXML, includeHTML);
    if (questionText != null) {
        parseResults[0] = "";
        parseResults[1] = questionType;
        parseResults[2] = questionText;
    } else {
        //Bad input
        importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "The question seems to be misformatted.", false);
        return null;
    }
    return parseResults;
}

//Parse a question type that has multiple answers
//Indexing is slightly different based on whether each answer has associated scores.
//hasScores is also true if the score cell is used for matching or groups
//Performs extra validation on true/false questions
//Returns an array representing the results of that parse, or null if it fails
function parseMultiQuestion(fileName, questionXML, questionType, sheet, hasScores, includeHTML) {
    var parseResults = new Array(Math.max.apply(null, dropDownCols));
    var questionText = parseQuestionText(questionXML, includeHTML);
    var answerArr = parseAnswersText(questionXML, questionType, includeHTML, fileName);

    if (questionText != null && answerArr != null) {
        //if T/F, answer array has 2 entries per answer - answer and score
        if (questionType == ("truefalse")) {
            if (answerArr.length != 4 || !(verifyTrueFalse(answerArr[0], answerArr[2]))) {
                //Not a valid true/false answer set
                importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "It is a true/false question but the answer options "
                                    + "do not match this question type.", false);
                return null;
            }
        }

        //The maximum number of answers QB2 supports limits imports from Moodle XML
        //Drop down columns for scores are 2 apart, so the following formula tells us how
        //many answers are supported
        var maxAnswers = (Math.max.apply(null, dropDownCols) - Math.min.apply(null, dropDownCols)) / 2 + 1;
        //Array has 2 entries per answer
        if (hasScores && answerArr.length > 2 * maxAnswers || !hasScores && answerArr.length > maxAnswers) {
            //Give only a warning and import the first few answers
            importQuestionFailed(fileName, parseQuestionName(questionXML), "(QB)^2 currently supports only " + maxAnswers + " answers, "
                                + "but this question has "
                                + (hasScores ? answerArr.length / 2 : answerArr.length)
                                + ". All answers were imported, but not all will export.", true);
        }

        parseResults[0] = "";
        parseResults[1] = questionType;

        // if multiple answer multiple choice, make question type multianswer
        if(questionType == "multichoice" && questionXML.getChildren("single").length == 1) {

            if (questionXML.getChildren("single")[0].getValue() == "false") {
                parseResults[1] = "multianswer";
            }

        }

        parseResults[2] = questionText;
        var hasAnswerMedia = false, hasAnswerImage = false, hasAnswerAudio = false;
        for (var i = 0; i < answerArr.length; i += (hasScores ? 2 : 1)) {
            // Check if the answer contains either image, audio, or media tag.
            // Depending on the tag, mark the corresponding boolean flag to be true.
            // The tag is added via function getAudioOrImageTag.
            if (answerArr[i] != null && answerArr[i].length > 6 ) {
                if (answerArr[i].substring(0, 6) == "[IMAGE") {
                    hasAnswerImage = true;
                } else if (answerArr[i].substring(0, 6) == "[AUDIO") {
                    hasAnswerAudio = true;
                } else if (answerArr[i].substring(0, 6) == "[MEDIA") {
                    hasAnswerMedia = true;
                }
            }
            //This ensures the proper spacing in the spreadsheet based on questionType
            parseResults[3 + i * (hasScores ? 1 : 2)] = answerArr[i];
            if (hasScores) {
                parseResults[4 + i] = answerArr[i + 1];
            }
        }

        // Add to Output depending on which flag is set.
        if (hasAnswerImage) {
            parseResults[2] = "[IMAGE IN ANSWERS] " + parseResults[2];
        } else if (hasAnswerAudio) {
            parseResults[2] = "[AUDIO IN ANSWERS]" + parseResults[2];
        } else if (hasAnswerMedia) {
            parseResults[2] = "[MEDIA IN ANSWERS] " + parseResults[2];
        }
    } else {
        //Bad input
        importQuestionFailed(fileName, parseQuestionName(questionXML), "The question seems to be misformatted.", false);
        return null;
    }
    return parseResults;
}

//Parse a cloze question
//Return the results of the parse, or null if failed
function parseClozeQuestion(fileName, questionXML, sheet, includeHTML) {
    var parseResults = new Array(3);
    var questionText = parseQuestionText(questionXML, includeHTML);
    //We'll trim this down to find each cloze qestion
    var copyOfQuestionText = questionText;

    if (questionText != null) {
        var clozeStartIndex = copyOfQuestionText.indexOf("{");
        var clozeEndIndex = copyOfQuestionText.indexOf("}");
        while (clozeStartIndex != -1 && clozeEndIndex != -1 && clozeStartIndex < clozeEndIndex) {
            //Each part of a cloze question is surrounded by braces in XML.
            //Treat each one separately
            var clozeText = copyOfQuestionText.substring(clozeStartIndex, clozeEndIndex + 1);
            copyOfQuestionText = copyOfQuestionText.substring(clozeEndIndex+1, copyOfQuestionText.length);

            var replaceString = parseClozePart(fileName, questionXML, includeHTML, clozeText);

            if (replaceString == null) {
                return null;
            }
            //Replace the Moodle format with the QB2 format
            questionText = questionText.replace(clozeText, replaceString);

            clozeStartIndex = copyOfQuestionText.indexOf("{");
            clozeEndIndex = copyOfQuestionText.indexOf("}");
        }

        parseResults[0] = "";
        parseResults[1] = "cloze";
        parseResults[2] = questionText;
    } else {
        //Bad input
        importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "The question seems to be misformatted.", false);
        return null;
    }
    return parseResults;
}

//Parse a single section of a cloze question
//Returns null if the part is invalid
function parseClozePart(fileName, questionXML, includeHTML, clozeText) {
    // Get clozeOption from DocumentProperties
    var properties = PropertiesService.getDocumentProperties();
    var clozeOption = properties.getProperty("clozeOption");

    var clozeType;
    var clozeTypeStartIndex = clozeText.indexOf(":");
    var clozeTypeEndIndex;
    //Get the text between the first two colons, which describe the type of cloze question
    if (clozeTypeStartIndex != -1) {
        clozeTypeEndIndex = clozeText.substring(clozeTypeStartIndex + 1, clozeText.length).indexOf(":");
        if (clozeTypeEndIndex != -1) {
            clozeType = clozeText.substring(clozeTypeStartIndex + 1, clozeTypeEndIndex + clozeTypeStartIndex + 1).trim();

            //Moodle can export many more formats than QB2 can handle.  Simplify and give errors if needed.
            if (clozeType.indexOf("MULTICHOICE") != -1 || clozeType.indexOf("MC") != -1) {
                clozeType = "MC";
            } else if (clozeType.indexOf("SHORTANSWER") != -1 || clozeType.indexOf("SA") != -1 || clozeType.indexOf("MW") != -1) {
                clozeType = "SA";
            } else {
                importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "The cloze question has an unsupported format.", false);
                return null;
            }

            //Get the answers from the XML
            var answersText = clozeText.substring(clozeTypeEndIndex + clozeTypeStartIndex + 2, clozeText.length - 1).trim();
            var answersArr = answersText.split("~");
            var replaceString = "";
            var wrongAnswers = new Array(answersArr.length - 1);
            var wrongAnswerCount = 0;
            for (var i = 0; i < answersArr.length; i++) {
                //For each answer we'll build the string to enter into QB2
                var answer = answersArr[i];
                var isRight;
                //Remove feedback from answers - unsupported by QB2.
                var commentLocation = answer.indexOf("\#");
                if (commentLocation != -1) {
                    answer = answer.substring(0, commentLocation);
                }
                if (answer.length > 0 && answer.substring(0, 1) == ("=")) {
                    //Right answer
                    answer = answer.substring(1, answer.length);
                    isRight = true;
                } else if (answer.length > 5 && answer.substring(0, 5) == ("%100%")) {
                    //Right answer
                    answer = answer.substring(5, answer.length);
                    isRight = true;
                } else if (answer.length > 0 && answer.substring(0, 1) == ("%")) {
                    //wrong answer with score listed
                    isRight = false;
                    var secondPercentIndex = answer.substring(1, answer.length).indexOf("%");
                    if (secondPercentIndex != -1) {
                        //Trim the score for this answer
                        answer = answer.substring(secondPercentIndex + 2);
                    }
                } else {
                    //wrong answer
                    isRight = false;
                }

                if (isRight) {
                    if (replaceString == ("")) {
                        if(clozeOption.equals("true")){
                            replaceString = "[[" + answer;
                        } else {
                            replaceString = "^" + answer;
                        }
                    } else {
                        //QB2 doesn't support multiple right answers
                        importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "The cloze question has multiple correct answers,"
                                            + " but (QB)^2 only supports 1 correct answer per question.", false);
                        return null;
                    }
                } else {
                    if (clozeType == ("SA")) {
                        importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "(QB)^2 does not support feedback or "
                                        + "partial credit for short answer cloze questions.  Incorrect answers will be ignored.", true);
                    } else {
                        if (wrongAnswerCount == wrongAnswers.length) {
                            //No correct answers
                            importQuestionFailed(fileName, parseQuestionName(questionXML, includeHTML), "The cloze question seems not to have any correct answers.", false);
                            return null;
                        }
                        //add the answer to an array of wrong answers for later formatting
                        wrongAnswers[wrongAnswerCount] = answer;
                        wrongAnswerCount++;
                    }
                }
            }

            if (clozeType != ("SA")) {
                //multichoice questions should list all wrong answers after the right one
                for (var j = 0; j < wrongAnswerCount; j++) {
                    replaceString += (", " + wrongAnswers[j]);
                }
            }

            if(clozeOption.equals("true")){
                replaceString += "]]";
            } else {
                replaceString += "^";
            }

            return replaceString;
        }
    }
    return null;
}

//Parse a question of the category type
//Adds it to categories if not there already
//Returns the category name, or null if the function fails
function parseCategory(fileName, questionXML, sheet, includeHTML, categories) {
    var categoryName = parseCategoryText(questionXML, includeHTML);
    if (categoryName != null) {
        //Check to see if the category has already come up.  If not, add it to
        //the category dictionary with a value of false to signal it is new
        if (categoryName != "" && !(categoryName in categories)) {
            categories[categoryName] = false;
        }
    } else {
        //Misformatted input
        importErrors.push("Could not import category in file \"" + fileName + "\". The category seems to be misformatted.");
        return null;
    }
    return categoryName;
}

//Parses the category name from the XML. Returns the text
//or null if misformatted.
function parseCategoryText(questionXML, includeHTML) {
    var categoryXML = questionXML.getChildren("category");
    if (categoryXML.length != 0) {
        categoryXML = categoryXML[0];
        var categoryName = categoryXML.getChildren("text");
        if (categoryName.length != 0) {
            categoryName = categoryName[0].getText();
            if (categoryName != null) {
                //Most of the time, the XML category will start with $course$/ or $cat1$/.
                //Remove if $ is present
                categoryName = removeXMLFormatting(categoryName, includeHTML);
                var textStartIndex = categoryName.indexOf("$");
                if (textStartIndex != -1) {
                    categoryName = categoryName.substring(categoryName.indexOf("/") + 1, categoryName.length).trim();
                }
                //Remove top/ from the start of the category names, since export will
                //add this automatically
                if (categoryName.length >= 4 && categoryName.substring(0, 4) == 'top/') {
                    categoryName = categoryName.substring(4, categoryName.length).trim();
                }
                return categoryName;
            }
        }
    }
    //Misformatted input
    return null;
}

//Returns the name of the question for error logging purposes
function parseQuestionName(questionXML, includeHTML) {
    var questionNameXML = questionXML.getChildren("name");
    if (questionNameXML.length != 0) {
        questionNameXML = questionNameXML[0];
        var questionName = questionNameXML.getChildren("text");
        if (questionName.length != 0) {
            questionName = questionName[0].getText();
            if (questionName != null) {
                return removeXMLFormatting(questionName.trim(), includeHTML);
            }
        }
    }
    return "(unknown question)";
}

//Parses the text of the question from the XML of the full question. Returns the text
//or null if misformatted.
function parseQuestionText(questionXML, includeHTML) {
    var questionTextXML = questionXML.getChildren("questiontext");
    if (questionTextXML.length == 0) {
        return null;
    }
    questionTextXML = questionTextXML[0];
    var questionText = questionTextXML.getChildren("text");
    if (questionText.length == 0) {
        return null;
    }
    questionText = questionText[0];
    questionText = questionText.getValue();
    if (questionText == null) {
        return null;
    }

    //See if media is attached
    var tag =  getAudioOrImageTag(questionTextXML);
    if(tag.length == 0){
        isPlugin = false;
    }
    else{
        isPlugin = true;
    }
    questionText = tag + questionText;

    if(includeHTML == false) {
        questionText = getVideos(questionText);
    }

    return removeXMLFormatting(String(questionText).trim(), includeHTML);
}

//Returns the question type of questionXML, converting Moodle-specific names
//to the QB2 style names where needed.
function getQuestionType(questionXML) {
    if (questionXML == null) {
        return null;
    }
    var questionTypeAttr = questionXML.getAttribute("type");
    if (questionTypeAttr == null) {
        return null;
    }
    var questionType = questionTypeAttr.getValue();
    //rename questiontype to correct QB2 name
    questionType = questionType == 'ddwtos' ? 'dragdrop' : questionType;
    questionType = questionType == 'gapselect' ? 'missingwords' : questionType;
    return questionType;
}

//Parses the answer choices from the XML of the full question. Returns an array of
//the answers, with scores if relevant.
//Returns null if no answers or there is a formatting error.
function parseAnswersText(questionXML, questionType, includeHTML, fileName) {
    var answersXML = null;
    if (questionType == 'dragdrop') {
        answersXML = questionXML.getChildren("dragbox");
    } else if (questionType == 'missingwords') {
        answersXML = questionXML.getChildren("selectoption");
    } else if (questionType == 'matching') {
        answersXML = questionXML.getChildren("subquestion");
    } else {
        answersXML = questionXML.getChildren("answer");
    }
    if (answersXML.length == 0) {
        return null;
    }

    //Set up the empty array for answers and scores
    var answerArray;
    answerArray = new Array(answersXML.length * (questionType == 'ordering' ? 1 : 2));

    //Parse each answer
    for (var i = 0; i < answersXML.length; i++) {
        var leftSideAnswer = parseLeftSideAnswer(answersXML[i], questionType, includeHTML);
        if (leftSideAnswer == null) {
            return null;
        }
        answerArray[i * (questionType == 'ordering' ? 1 : 2)] = leftSideAnswer;

        if (questionType != 'ordering') {
            //All other question types have a second piece of information
            //score, group, or matching text
            var rightSideAnswer = parseRightSideAnswer(answersXML[i], questionType, includeHTML, fileName, parseQuestionName(questionXML, includeHTML));
            if (rightSideAnswer == null) {
                return null;
            }
            answerArray[i * 2 + 1] = rightSideAnswer;
        }
    }
    return answerArray;
}

//Parses the answer text portion of an answer, returning null if invalid
function parseLeftSideAnswer(answer, questionType, includeHTML) {
    var answerText = answer.getChildren("text");
    if (answerText.length == 0) {
        return null;
    }
    answerText = answerText[0].getText();
    if (answerText == null) {
        return null;
    }

    //parse feedback if present
    if (questionType == 'shortanswer' || questionType == 'multichoice' || questionType == 'truefalse') {
        var feedback = answer.getChild("feedback");
        if (feedback != null) {
            var feedbackText = feedback.getChildText("text");
            if (feedbackText != null && feedbackText != "") {
                answerText = answerText + " [FEEDBACK] " + feedbackText;
            }
        }
    }
    //See if media is attached
    var tag = getAudioOrImageTag(answer);
    if(tag.length == 0){
        isPlugin = false;
    }
    else{
        isPlugin = true;
    }
    answerText = tag + answerText;

    if(includeHTML == false) {
        answerText = getVideos(answerText);
    }

    return removeXMLFormatting(answerText.trim(), includeHTML);
}

//Parses the score/group/matching portion of an answer, returning null if invalid
function parseRightSideAnswer(answer, questionType, includeHTML, fileName, questionName) {
    var rightSide = "";
    var childName = "group";
    var subAnswer = answer;
    //Although there are similarities, the exact structure of the XML for each
    //question type is different.
    if (questionType == 'matching') {
        subAnswer = subAnswer.getChildren("answer");
        if (subAnswer.length == 0) {
            return null;
        }
        subAnswer = subAnswer[0];
        childName = "text";
    }
    if (questionType == 'dragdrop' || questionType == 'missingwords' || questionType == 'matching') {
        var groupText = subAnswer.getChildren(childName);
        if (groupText.length == 0) {
            return null;
        }
        groupText = groupText[0].getText();
        if (groupText == null) {
            return null;
        }
        rightSide = removeXMLFormatting(groupText.trim(), includeHTML);
    } else if (questionType == 'shortanswer' || questionType == 'multichoice' || questionType == 'truefalse') {
        var fraction = subAnswer.getAttribute("fraction");
        if (fraction == null) {
            //Accept "invalid" XML that does not give a score to certain answers.
            //Assume these answers are wrong.
            importQuestionFailed(fileName, questionName, "An answer in this question has no associated score. Automatically setting it to no credit.", true);
            return 0;
        }
        fraction = fraction.getValue();
        if (fraction == null || fraction == '') {
            //Accept "invalid" XML that does not give a score to certain answers.
            //Assume these answers are wrong.
            importQuestionFailed(fileName, questionName, "An answer in this question has no associated score. Automatically setting it to no credit.", true);
            return 0;
        }
        rightSide = fraction;
    }
    return rightSide;
}

/****************************************************
 * The following functions are helper functions
 * to process and format questions
 ****************************************************/

//checks that both truefalse answer options are valid
//accounts for feedback
//Returns true if the input is valid and false otherwise
function verifyTrueFalse(answerArr1, answerArr2) {
    var TFTextFirst = answerArr1;
    var TFTextSecond = answerArr2;
    if (answerArr1.indexOf("[FEEDBACK]") != -1) {
        var index = answerArr1.indexOf("[FEEDBACK]");
        TFTextFirst = answerArr1.substring(0, index).trim();
    }
    if (answerArr2.indexOf("[FEEDBACK]") != -1) {
        index = answerArr2.indexOf("[FEEDBACK]");
        TFTextSecond = answerArr2.substring(0, index).trim();
    }
    if (TFTextFirst.toLowerCase() == "true" && TFTextSecond.toLowerCase() == "false") {
        return true;
    }
    if (TFTextFirst.toLowerCase() == "false" && TFTextSecond.toLowerCase() == "true") {
        return true;
    }
    return false;
}

// Replaces instances of embedded videos with
// [VIDEO] tag followed by their title and unique hash
function getVideos(xmlString) {
    
    // the regexes for videos with titles will follow these formats:
    // <a href="http://kaltura-kaf-uri.com/browseandembed/index/media/entryid/0_p8z9fwbo/showDescription/false/showTitle/false/showTags/false/showDuration/false/showOwner/false/showUploadDate/false/playerSize/608x402/playerSkin/26365392/"tinymce-kalturamedia-embed||earth (00:31)||608||402</a>
    // <a href="http://kaf.ccle.ucla.edu/browseandembed/index/media/entryid/0_e76a374z/showDescription/false/showTitle/false/showTags/false/showDuration/false/showOwner/false/showUploadDate/false/playerSize/608x402/playerSkin/26365392/">tinymce-kalturamedia-embed||earth (00:31)||608||402</a>
    // <a href="http://kaf-test.ccle.ucla.edu/browseandembed/index/media/entryid/0_e76a374z/showDescription/false/showTitle/false/showTags/false/showDuration/false/showOwner/false/showUploadDate/false/playerSize/608x402/playerSkin/26365392/">tinymce-kalturamedia-embed||earth (00:31)||608||402</a>

    // the regexes for videos without titles will follow these formats
    // <a href="http://kaltura-kaf-uri.com/browseandembed/index/media/entryid/0_p8z9fwbo/showDescription/false/showTitle/false/showTags/false/showDuration/false/showOwner/false/showUploadDate/false/playerSize/608x402/playerSkin/26365392/"></a>
    // <a href="http://kaf.ccle.ucla.edu/browseandembed/index/media/entryid/0_e76a374z/showDescription/false/showTitle/false/showTags/false/showDuration/false/showOwner/false/showUploadDate/false/playerSize/608x402/playerSkin/26365392/"></a>
    // <a href="http://kaf-test.ccle.ucla.edu/browseandembed/index/media/entryid/0_e76a374z/showDescription/false/showTitle/false/showTags/false/showDuration/false/showOwner/false/showUploadDate/false/playerSize/608x402/playerSkin/26365392/"></a>

    // regex that matches the anchor tag that embeds the video
    // has capture groups for hash, the inner string, and the title of the video
    // follows format "[VIDEO _title_ _hash_]"
    var regexTitle = /<a href='http:\/\/(?:kaf(?:-test)?.ccle.ucla.edu)?(?:kaltura-kaf-uri.com)?\/browseandembed\/index\/media\/entryid\/(.*?)\/.*?(tinymce-kalturamedia-embed\|\|([\s\S]*?)\((?:\d*:)?\d*:\d*\)\|\|(\d*)\|\|(\d*))<\/a>/;
    var regexNoTitle = /<a href='http:\/\/(?:kaf(?:-test)?.ccle.ucla.edu)?(?:kaltura-kaf-uri.com)?\/browseandembed\/index\/media\/entryid\/(\S*?)\/.*?(><\/a>)/;

    // replace inner string with tag for videos with titles
    while(regexTitle.test(xmlString) == true) {

        var hash = xmlString.match(regexTitle)[1];
        var toReplace = xmlString.match(regexTitle)[2];
        var title = xmlString.match(regexTitle)[3];
        // width and height are important for moodle, as it parses these to determine the video resolution, even though it's just alt text.
        var width = xmlString.match(regexTitle)[4]; 
        var height = xmlString.match(regexTitle)[5];

        var newString = "[VIDEO " + title + " " + hash + " " + width + "|" + height  + "]";
        xmlString = xmlString.replace(toReplace, newString);


    }

    // replace inner string with tag for videos without titles
    while(regexNoTitle.test(xmlString) == true) {

        var toReplace = xmlString.match(regexNoTitle)[2];
        var hash = xmlString.match(regexNoTitle)[1];

        var newString = ">[VIDEO " + hash + " " + width + "|" + height + "]</a>";
        xmlString = xmlString.replace(toReplace, newString);

    }

    return(xmlString);

}

//If the Moodle XML contains media (given with the file tag),
//then generate a set of tags to put at the start of the question
//to flag this for users.  Otherwise, return an empty string.
//xmlString is the subsection of XML that may have file as a child
function getAudioOrImageTag(xmlString) {
    var media = xmlString.getChildren("file");
    if (media.length != 0) {
        var mediaText = '['
        for (var j = 0; j < media.length; j++) {
            var file = media[j].getAttribute("name");
            if (file != null) {
                file = file.getValue();
                if (isImage(file)) {
                    mediaText += "IMAGE ";
                } else if (isAudio(file)) {
                    mediaText += "AUDIO ";
                } else {
                    mediaText += "MEDIA ";
                }
                mediaText += (file + ", ");
            }
        }
        mediaText = mediaText.substring(0, mediaText.length - 2) + "] ";
        return mediaText;
    } //else
    return "";
}

// Determine whether a file is image or not by checking its file extension through Regular Expression.
function isImage(file) {
    var image_extensions = ['jpg', 'jpeg', 'gif', 'png', 'svg', 'webp', 'heic', 'pdf'];
    return image_extensions.some(
        function(extension) {
            return RegExp("^.*." + extension + "$", "i").test(file);
        }
    )
}

// Determine whether a file is audio or not by checking its file extension through Regular Expression.
function isAudio(file) {
    var audio_extensions = ['aac', 'aif', 'aiff', 'iff', 'm3u', 'm4a', 'mid', 'mp3', 'mpa', 'wav', 'wmv'];
    return audio_extensions.some(
        function(extension) {
            return  RegExp("^.*." + extension + "$", "i").test(file);
        }
    )
}

//includeHTML determines if we should strip all HTML tags
function removeXMLFormatting(xmlString, includeHTML) {
    if (xmlString == null) {
        return xmlString;
    }
    var hasMoodleHTML = false;
    //Remove HTML if desired
    if (!includeHTML) {
        //for Moodle but not (QB)2 output, a distinction may be made
        //between HTML < and > signs and user-input ones (&lt;).
        //We therefore want to strip tags before converting &lt; to >
        var ltStartIndex = xmlString.indexOf("<");
        var gtStartIndex = xmlString.indexOf(">");
        //We have < and > tags so this is Moodle output
        if (ltStartIndex != -1 && gtStartIndex != -1) {
            xmlString = stripHTML(xmlString);
            //Flag that we've already stripped HTML tags so we don't again later
            hasMoodleHTML = true;
        }
    }

    //Improve readability
    xmlString = xmlString.replace(/&gt;/g, ">");
    xmlString = xmlString.replace(/&lt;/g, "<");
    xmlString = xmlString.replace(/&quot;/g, "\"");
    xmlString = xmlString.replace(/&apos;/g, "\'");
    xmlString = xmlString.replace(/&amp;/g, "&");
    xmlString = xmlString.replace(/&nbsp;/g, " ");

    //Remove HTML if desired and we haven't already done so above
    //Necessary for importing files from (QB)^2.
    if (!hasMoodleHTML && !includeHTML) {
      xmlString = stripHTML(xmlString);
    }
    return xmlString;
}

//Removes HTML tags from the input string
function stripHTML(xmlString) {
  
    // First, check if our Question/Description contains img tag with alt, src, and dimensions.
    const imgRegex = /<img ([a-z]+="[^"]*"\s*)*>/g;
    const found = xmlString.match(imgRegex);
  
    // If Question/Description contains img tag, we replace it with the format we want, and continue with
    // deleting unnecessary html tags.
    if (found && !isPlugin ) {
      xmlString = xmlString.replace(/<img ([a-z]+="[^"]*"\s*)*>/g, function(ele){
        var addString = "[IMAGE | ";
        const splitArr = ele.match(/[a-z]+="[^"]*"/g);
        if (splitArr) {
          for(var i = 0; i < splitArr.length; i++) {
            if(splitArr[i]){
              var attrString = splitArr[i].split("=");
              var attrName = attrString[0], attrValue = attrString[1];
              if(allowedAttrs.indexOf(attrName) != -1){
                 addString += attrName + ": " + attrValue + " | ";
              }
            }
          }
        }
        addString += "]";
        return addString;
      });
    }
  
    // Clean up html tags
    xmlString = xmlString.replace(/<br>/g, "\n");
    xmlString = xmlString.replace(/<\/?[a-zA-Z][^>]*>/g, "");
    return xmlString;
}

/****************************************************
 * The following functions handle error and
 * success messages
 ****************************************************/

//Adds an error message that the input file could not be parsed
function importFileFailed(fileName, errorMsg) {
    errorMsg = 'The file ' + fileName + ' could not be imported. ' + errorMsg;
    //This is a major issue affecting a full file, so put it at the front of the error list
    importErrors.unshift(errorMsg);
}

//Add a note that the import of a certain question failed or generated a warning.
function importQuestionFailed(fileName, questionName, errorMsg, isWarning) {
    //Generate the full error or warning message
    if (isWarning) {
        errorMsg = "Warning in file \"" + fileName + "\" for question \"" + questionName + "\".\n" + errorMsg;
    } else {
        errorMsg = "Error in file \"" + fileName + "\". Could not import question \"" + questionName + "\".\n" + errorMsg;
    }
    importErrors.push(errorMsg);
}

//Displays a message indicating that import is complete
function showImportCompleteMessage(filesRead) {
    var htmlText = "";
    var html = null;
    var title = "";

    if (filesRead > 0 && importErrors.length == 0) {
        //Import succeeded completely
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
            'Moodle Import Complete!',
            'The Moodle XML ' + (filesRead >= 2 ? 'sheets have' : 'sheet has') + ' been successfully imported.',
            ui.ButtonSet.OK);
        return;
    } else if (filesRead > 0 && importErrors.length != 0) {
        //Import succeeded partially. List questions that failed to import.
        writeErrors();
        htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">'
                    + 'The Moodle XML ' + (filesRead >= 2 ? 'sheets have' : 'sheet has') + ' been successfully imported.<br>'
                    + 'However, some questions could not be added:<br>'
                    + 'Details are listed below and in the \'Import Errors\' tab at the bottom of this spreadsheet.</p>'
                    + '<textarea readonly rows="7" style="width:100%; resize:none">';
        for (var i = 0; i < importErrors.length; i++) {
            htmlText += importErrors[i] + '\n\n';
        }
        htmlText = htmlText.trim();
        htmlText += '</textarea>';
        htmlText += okButtonHTML;
        html = HtmlService.createHtmlOutput(htmlText).setWidth(500).setHeight(260);
        title = 'Moodle Import Complete!';
    } else {
        //Import failed.  List reasons if possible.
        var height = 90;
        htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">'
                    + 'The selection does not appear to contain any valid Moodle XML files.';
        if (importErrors.length > 0) {
            writeErrors();
            htmlText += '<br>Details are listed below and in the \'Import Errors\' tab at the bottom of this spreadsheet.</p>'
                        + '<textarea readonly rows="7" style="width:100%;">';
            for (var i = 0; i < importErrors.length; i++) {
                htmlText += importErrors[i] + '\n\n';
            }
            htmlText = htmlText.trim();
            htmlText += '</textarea>';
            height = 260;
        }
        htmlText += okButtonHTML;

        html = HtmlService.createHtmlOutput(htmlText).setWidth(500).setHeight(height);
        title = 'Import Error !';
    }

    SpreadsheetApp.getUi().showModalDialog(html, title);
}

/****************************************************
 * The following functions write to spreadsheets
 ****************************************************/

//Write all categories in the input not already in the spreadsheet to the categories tab
function writeCategories(sheet, categories) {
    if (categories == null) {
        return;
    }
    var categoryLen = Object.keys(categories).length;

    var categorySheet = SpreadsheetApp.getActive().getSheetByName("Categories");
    var row = categorySheet.getDataRange().getNumRows();

    if (row != 1 || !categorySheet.getRange(row, 1).getDisplayValue() == ("")) {
        //The first category added to a sheet has to be special cased so the first row is used
        //Otherwise, we increment by 1 to get the first empty row
        row++;
    }

    //Add rows to this sheet if there are too many new categories
    //If MaxRows is 3 and row is 1 we can add 3 categories.
    if (row + categoryLen - 1 >= categorySheet.getMaxRows()) {
        //If the sheet is entirely full already, we have to special case where we add the new rows
        //Ensure 10 blank rows at the end
        categorySheet.insertRowsAfter((row > categorySheet.getMaxRows() ? row - 1 : row), categoryLen - (categorySheet.getMaxRows() - row + 1) + 10);
    }

    // makes 2D array for input into .setValues()
    var cats = [];
    for (category in categories) {
        if (!(categories[category])) {
            var temp = [];
            temp.push(category);
            cats.push(temp);
        }
    }

    // inputs new categories; starts at lowest empty row, goes length of new categories
	if (cats.length != 0) {
		categorySheet.getRange(row, 1, cats.length, 1).setValues(cats).setHorizontalAlignment(defaultAlignment);
	}
}

//Write all questions to the Question Bank sheet
function writeQuestions(sheet, questionArr) {
    if (questionArr == null) {
        return;
    }

    //Insert as the question in the final row
    var range = sheet.getDataRange();
    var values = range.getValues();
    var row = range.getNumRows();
    if (sheet.getRange(row, 3).getDisplayValue() != "") {
        //If there are questions, this returns the final row with questions
        //So we need to increment by 1
        row++;
    }

    //Add rows to this sheet if there are too many new questions
    //If MaxRows is 3 and row is 1 we can add 3 questions.
    if (row + questionArr.length - 1 >= sheet.getMaxRows()) {
        //If the sheet is entirely full already, we have to special case where we add the new rows
        //Ensure 20 blank rows at the end
        sheet.insertRowsAfter((row > sheet.getMaxRows() ? row - 1 : row), questionArr.length - (sheet.getMaxRows() - row + 1) + 20);
    }

    var rowStart = row;

    for (var i = 0; i < questionArr.length; i++) {
        var question = questionArr[i];
        if (question == null) {
            continue;
        }
        dropDownCols.forEach(function(ddcol) {
            sheet.getRange(row,ddcol).clearDataValidations();
        });

        var cells = [];

        for (var col = 0; col < question.length; col++) {
            if (question[col] != null) {
                cells.push(question[col]);
            } else {
                cells.push("");
            }
        }

        var inputCells = [cells];
        sheet.getRange(row, 1, 1, question.length).setWrap(true).setValues(inputCells).setHorizontalAlignment(defaultAlignment);
        row++;
    }

    //Set unneeded cells grey
    colorvals = sheet.getRange(rowStart, 2, questionArr.length, 1).getValues();

    for (var i = 0; i < questionArr.length; i++) {
        prepareDataValidation(sheet, rowStart + i, colorvals[i][0], false);
    }
}

//Write errors to the Import Errors sheet
function writeErrors() {
    //Get the Import Errors sheet, or create it as the last sheet if it doesn't exist
    var activeSpreadsheet = SpreadsheetApp.getActive();
    var errorSheet = activeSpreadsheet.getSheetByName("Import Errors");
    if (errorSheet == null) {
        var activeSheet = activeSpreadsheet.getActiveSheet();
        errorSheet = activeSpreadsheet.insertSheet("Import Errors");
        errorSheet.setColumnWidth(1, 500).setRowHeight(1, 60);

        //Deleting extra empty columns
        var firstCol = 2;
        var colsToDelete = errorSheet.getMaxColumns() - firstCol + 1;
        if (colsToDelete >= 1) {
            errorSheet.deleteColumns(firstCol, colsToDelete);
        }
        errorSheet.setTabColor(red);
        //Inserting a sheet switches over to it, so switch back.
        activeSheet.activate();
    }

    //Set the first row to be a bold message alerting the user to errors
    var headerStyle = SpreadsheetApp.newTextStyle()
    .setForegroundColor(white)
    .setFontSize(11)
    .setBold(true).build();

    errorSheet.getRange(1, 1)
    .setDataValidation(null)
    .clear()
    .setValue("Errors found during import:")
    .setTextStyle(headerStyle)
    .setBackground(red)
    .setHorizontalAlignment("center")
    .setVerticalAlignment("middle");

    //Add rows to this sheet if there are too many errors
    //If MaxRows is 3 we can add 3 error messages.
    if (importErrors.length - 1 >= errorSheet.getMaxRows()) {
        errorSheet.insertRowsAfter(1, importErrors.length - errorSheet.getMaxRows());
    }

    //Put the error messages in the spreadsheet for the user to reference later
    var row = errorSheet.getDataRange().getNumRows();
    if (row != 1 || !errorSheet.getRange(row, 1).getDisplayValue() == ("")) {
        //The first row will have this index, but all others need to be incremented by 1
        row++;
    }
    for (var i = 0; i < importErrors.length; i++) {
        errorSheet.getRange(row, 1).setValue(importErrors[i]).setHorizontalAlignment(defaultAlignment);
        row++;
    }
}


// MOODLE BACKUP IMPORT

//Import moodle backup file into document
function importDocumentMoodleBackup(file, activeSheetName, includeHTML) {

    var fileName = file.getName();

    try {
        //Read the XML file as a string
        //Ensure the file can be parsed as text
        var importContent = "";
        var document;
        try {
            //Moodle and QB2 use different quotation marks
            importContent = file.getBlob().getDataAsString("UTF-8").trim();
            document = XmlService.parse(importContent);
        } catch(e) {
            importFileFailed(fileName, "Google was unable to read the file as valid XML. It returned the following error message.\n" + e);
            return false;
        }

        var courseName = document.getRootElement().getChild("information").getChild("original_course_fullname").getText();

        var activities = document.getRootElement().getChild("information").getChild("contents").getChild("activities").getChildren("activity");
        if (activities == null || activities.length == 0) {
            return false;
        }

        var activitiesText = [];

        for (var i = 0; i < activities.length; i++) {

            activitiesText[i] = activities[i].getChild("title").getText();

        }

        createDocumentMoodleBackup(courseName, activitiesText);

    } catch (e) {
        importFileFailed(fileName, "We were unable to parse your file as Moodle Backup XML.  Please make sure it has been exported correctly.");
        return false;
    }
    return true;
}

// write the moodle backup document
function createDocumentMoodleBackup(title, activities) {
    Logger.log(title);
    Logger.log(activities);
    var results = createDocument(title + " - Moodle Backup Import", true);
    var docID = results[0];
    var folderID = results[1];
    var doc = DocumentApp.openById(docID);
    createHeaderAndFooter(doc);
    exportMoodleBackupPlaintext(doc.getBody().setPageHeight(792).setPageWidth(612), activities);
    doc.saveAndClose();
    displayResult(doc, folderID, 0);

}

// write the list for the moodle backup
function exportMoodleBackupPlaintext(body, activities) {


    for(var i = 0; i < activities.length; i++) {
    
        body.insertListItem(i+2, activities[i]).setGlyphType(DocumentApp.GlyphType.BULLET);

    }

}