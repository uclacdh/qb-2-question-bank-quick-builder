/*
(QB)² - Question Bank Quick Builder

The Clear BSD License

Copyright (c) 2021 Regents of the University of California
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted (subject to the limitations in the disclaimer
below) provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

     * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     * Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

//Preferred default settings based on question type
var defaultSettings = new Array();
defaultSettings['ordering'] = {
    selecttype: 'ALL',
    gradingtype: 'ABSOLUTE_POSITION'
};
defaultSettings['poodllrecording'] = {
    responseformat: 'AUDIO'
};

//original column headers; using these values so user is free to change the header on spreadsheet without breaking the code.
var columnHeaders = ['category', 'type', 'questiontext', 'answer', 'fraction', 'answer', 'fraction',
    'answer', 'fraction', 'answer', 'fraction', 'answer', 'fraction', 'answer', 'fraction'
];

//columns to updated that are fraction/group/etc (starts at 1)
var dropDownCols = [5, 7, 9, 11, 13, 15];

//Export code
function exportMoodle(rootElement, replace, activeSheetName, shuffle) {
    showCompilingMessage('Compiling Moodle XML...', 'This may take a few minutes...', 350);

    exportSpreadsheetMoodle(rootElement, replace, activeSheetName, shuffle);
}

//helper export function that builds XML content
function exportSpreadsheetMoodle(rootElement, replaceFile, activeSheetName) {
    var properties = PropertiesService.getDocumentProperties();
    var shuffle = properties.getProperty('shuffleCheckbox');
    if (shuffle == null) {
        shuffle = true;
        properties.setProperty('shuffleCheckbox', true);
    } else {
        shuffle = (shuffle == "true");
    }

    //We export from the active sheet, but this is undesirable if the active sheet is not a question bank
    //Let the user know they need to change sheets if they are on one of the following
    if (activeSheetName == 'Categories' || activeSheetName == 'QuestionTypes' || activeSheetName == 'QuestionOptions' || activeSheetName == 'Import Errors') {
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
            'Switch to Question Bank',
            'Export is performed on the sheet that is currently visible.\nPlease navigate to your question bank and try again.',
            ui.ButtonSet.OK);
        return;
    }
    var spreadsheet = SpreadsheetApp.getActive();
    var sheet = spreadsheet.getSheetByName(activeSheetName);
    var fileName = spreadsheet.getName() + ".xml";

    //create root element of XML file
    //rootElement is <quiz>
    var root = XmlService.createElement(rootElement);

    var range = sheet.getDataRange();
    var values = range.getValues();
    var numRows = range.getNumRows();

    var prevCategory = "";
    var prevQuestionType = "";

    try {
        //Catch errors at the sheet level to provide a popup letting the user know export failed.
        //r = 2 because first two rows are reserved for column header and extra space for notes that arent needed for compiling.
        for (var r = 2; r < numRows; r++) {
            try {
                //Catch errors at the question level so we can provide users with the line they occur on

                // If the previous Category is the same as this one, we will not print it out.
                // If the Category changes, we only print it out once, according to the CCLE format.
                var shouldAdd = false;
                //cast to string in case cell is formatted as a number
                var category = String(values[r][0]);
                // Get question type
                var questionType = String(values[r][1]);
                // Get question Text (Don't allow empty question Text)
                var questionText = String(values[r][2]);
                if(questionText == "" && questionType == ""){
                    continue;
                }
                // If empty category at the start, give it an empty category
                if(category == '' && prevCategory == ''){
                    var categoryrow = getCategory('');
                    shouldAdd = true;
                }else if(category != prevCategory && category == ''){
                    category = prevCategory;
                }else if(category != prevCategory){
                    prevCategory = category;
                    shouldAdd = true;
                    var categoryrow = getCategory(category);
                }

                // If questionType is empty, inherit the previous
                if(questionType == "" && prevQuestionType == "") {
                    continue;
                } else if(questionType != prevQuestionType && questionType == "") {
                    questionType = prevQuestionType;
                } else if(questionType != prevQuestionType) {
                    prevQuestionType = questionType;
                }

                // The condition of shouldAdd ensures that one category is only being produced once.
                if(shouldAdd){
                    root.addContent(categoryrow);
                }


                // Get question type
                var questionType = values[r][1];

                // set variable to flag multianswer question in order to add <single> tag
                var isMultians = false;
                if(questionType == "multianswer") {
                    isMultians = true;
                    questionType = 'multichoice';
                }

                //rename questiontype to correct xml name
                questionType = questionType == 'dragdrop' ? 'ddwtos' : questionType;
                questionType = questionType == 'missingwords' ? 'gapselect' : questionType;

                //question type information
                //create question element with attribute "type" that will be parent element of all question body elements
                var questionTypeElement = XmlService.createElement('question')
                    .setAttribute('type', questionType);
                //add question type element as child of root element quiz
                root.addContent(questionTypeElement);

                //question name and text information
                // create child <name> and <questiontext> elements of parent questionTypeElement element
                // Special case for cloze questiontype
                if (questionType != 'cloze') {
                    getNameandText(questionTypeElement, values[r][2]);
                }

                //question answer options information
                //questionTypeElement is the parent element of all the answer option elements
                switch (questionType) {
                    case 'multichoice':
                        if(isMultians) {
                            singleElement = XmlService.createElement("single").setText("false");
                            questionTypeElement.addContent(singleElement);
                        }
                    case 'shortanswer':
                    case 'truefalse':
                        buildFractionQuestions(questionTypeElement, questionType, values[r]);
                        break;
                    case 'ordering':
                        buildOrderingQuestions(questionTypeElement, questionType, values[r]);
                        break;
                    case 'matching':
                        buildMatchingQuestions(questionTypeElement, values[r]);
                        break;
                    case 'ddwtos':
                    case 'gapselect':
                        buildGroupQuestions(questionTypeElement, values[r], questionType);
                        break;
                    case 'cloze':
                        //cloze has a special format
                        var clozeValue = reformatClozeQuestions(values[r][2]);
                        getNameandText(questionTypeElement, clozeValue, true);
                        break;
                    case 'poodllrecording':
                        addGraderInfoSetting(questionTypeElement);
                    default: //essay and description answers do not need to built
                        break;
                }

                if(questionType == 'multichoice' || questionType == 'matching' ||
                   questionType == 'ddwtos' || questionType == 'gapselect') {
                    addShuffleSetting(questionTypeElement, shuffle);
                }

                //insert setting elements
                addDefaultSettings(questionTypeElement, questionType);

            } catch (e) {
                throw "An error occurred while processing the question in row " + (r + 1) + " that prevented export from completing. " +
                    "Try removing this question and exporting again.";
            }
        } // End row for loop

        try {
            //Creates an XML document with <quiz> as the root node
            var document = XmlService.createDocument(root);
            //Formats the XML into a human-readable XML document
            var rawValue = XmlService.getPrettyFormat().setOmitDeclaration(true).format(document);

            //writes XML document to Google Drive
            exportDocument(fileName, rawValue, ContentService.MimeType.XML, replaceFile);
        } catch (e) {
            throw "An error occurred while writing the XML sheet to your Google Drive.";
        }
    } catch (e) {
        e = e.toString(); //Cast to string in case the exception is not of type string
        var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' + e;
        //Variable height based on error length
        var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(50 + 15 * Math.ceil((e.length - 67) / 67));
        SpreadsheetApp.getUi().showModalDialog(html, 'Moodle Export Failed');
    }
}

//creates a file from content in Google Drive
function exportDocument(filename, content, type, replaceFile) {

    //Creates the document and moves it into the same folder as the original file
    var file = DriveApp.createFile(filename, content);
    var currentFileId = SpreadsheetApp.getActive().getId();
    var currentFiles = DriveApp.getFilesByName(SpreadsheetApp.getActive().getName());
    var currentFile;

    while (currentFiles.hasNext()) {

        currentFile = currentFiles.next();

        if (currentFile.getId() == currentFileId) break;
    }

    var folders = currentFile.getParents();

    var parentFolder;
    while (folders.hasNext()) {
        parentFolder = folders.next();
    }

    if (parentFolder != null) {
        var newFile;

        if (replaceFile) {
            newFile = file.makeCopy(file.getName(), parentFolder);
            file.setTrashed(true);

            currentFiles = parentFolder.getFiles();

            while (currentFiles.hasNext()) {

                currentFile = currentFiles.next();

                if (currentFile.getName() == newFile.getName() && currentFile.getId() != newFile.getId()) {
                    try {
                        currentFile.setTrashed(true);
                    }
                    catch (e) {
                        //User does not have permissions to delete the old XML sheet.
                        continue;
                    }
                }
            }
        } else {
            newFile = file.makeCopy(file.getName(), parentFolder);
            file.setTrashed(true);
        }

        var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' +
            '<style>.display { width:355px; height:85px; text-align: center; overflow: auto; } </style>' +
            'File exported successfully. You can view the file here:' +
            '<div class="display"><br><br><a href="' + newFile.getUrl() + '" target="_blank">' + newFile.getName() + '</a></div>';

        var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(100);

        SpreadsheetApp.getUi().showModalDialog(html, 'Moodle Export Complete!');
    } else {
        //Could not access containing folder for writing so leave it in user's root
        var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' +
            '<style>.display { width:355px; height:95px; text-align: center; overflow: auto; } </style>' +
            'File exported successfully. Since you do not appear to be the owner of the spreadsheet, the file ' +
            'has been saved in your own Google Drive. You can view the file here:' +
            '<div class="display"><br><br><a href="' + file.getUrl() + '" target="_blank">' + file.getName() + '</a></div>';

        var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(130);

        SpreadsheetApp.getUi().showModalDialog(html, 'Moodle Export Complete!');
    }
}

//creates question element with attribute "category" and child elements
function getCategory(value) {
    //cast to string in case cell is formatted as a number
    value = String(value);

    var questionParentElement = XmlService.createElement('question')
        .setAttribute('type', 'category');
    //create child category element of question
    var categoryChildElement = XmlService.createElement('category');
    questionParentElement.addContent(categoryChildElement);
    //create child text element for course name of category element
    var textCourseElement = XmlService.createElement('text').setText("$course$/top/" + value);
    categoryChildElement.addContent(textCourseElement);
    return questionParentElement;
}

//create child <name> and <questiontext> elements of parent element questionTypeElement
function getNameandText(questionTypeElement, value, cloze) {
    value = String(value); //cast to string in case spreadsheet cell is of type number
    cloze = cloze || false; //optional param set here because gs doesnt like it being set in param
    const toChangeRegex = /\[IMAGE[^\]]*]/g;

    var name = value.substring(0, 45);

    if (cloze) {
        name = name.indexOf('{') > -1 ? name.substr(0, name.indexOf('{')) : name; //only show text before answers
    }

    var nameElement = XmlService.createElement('name');
    var name_TextElement = XmlService.createElement('text').setText(name);
    //add child text element to parent name element
    nameElement.addContent(name_TextElement);
    //add child name element to parent question type element
    questionTypeElement.addContent(nameElement);
    // strip video tag and turn into kaltura embed
    value = addVideo(value);

    var questiontextElement = XmlService.createElement('questiontext')
        .setAttribute('format', 'html');

    if(value.match(toChangeRegex)){
        value = buildImgTag(value);
    }

    var qtext_TextElement = XmlService.createElement('text').setText(value);

    //add child text element to parent question text element
    questiontextElement.addContent(qtext_TextElement);
    //add child question text element to parent question type element
    questionTypeElement.addContent(questiontextElement);
}

// Construct img tag from our [IMAGE ... ] format
function buildImgTag(value) {
    // replace [IMAGE ... ] to <img src=... alt=... width=... height=...>
    const toChangeRegex = /\[IMAGE[^\]]*\| ]/g;
    if(value.match(toChangeRegex)){
        return value.replace(toChangeRegex, function(ele) {
            const splitArr = ele.split(" | ");
            var result = "<img ";
            for(var i = 1 ; i < splitArr.length-1; i++){
                var attrs = splitArr[i].split(": ");
                result += attrs[0] + "=" + attrs[1] + " ";
            }
            result += ">";
            return result
        });
    }else
        return value;
}

//build answer options for ordering questions
function buildOrderingQuestions(questionParentElement, questionType, rowValues) {

    var startCol = 3; //start at the question

    for (var col = startCol; col < columnHeaders.length; col++) {

        var header = String(columnHeaders[col]);

        if (header === 'answer') {

            var answer = String(rowValues[col]);
            answer = addVideo(answer);

            if (/\S/.test(answer)) { //if answer has non white space characters
                //create basic answer element
                var answerElement = XmlService.createElement('answer')
                    .setAttribute('format', 'html');
                //ordering has a simpler XML format than the other question types

                //feedback only supported for multiple choice, short answer, and truefalse
                //Strip feedback from non-supported question types
                answer = buildImgTag(answer);
                answer = stripFeedback(answer);
                var textElement = XmlService.createElement('text').setText(answer);
                answerElement.addContent(textElement);
                questionParentElement.addContent(answerElement);
            }
        }
    }
}

//build answer option elements for multiple choice, short answer, truefalse
function buildFractionQuestions(questionParentElement, questionType, rowValues) {

    var startCol = 3; //start at the question

    for (var col = startCol; col < columnHeaders.length; col++) {

        var header = String(columnHeaders[col]);

        if (header === 'answer') {

            var answer = String(rowValues[col]);
            answer = addVideo(answer);

            if (/\S/.test(answer)) { //if answer has non white space characters
                //create basic answer element
                var answerElement = XmlService.createElement('answer')
                    .setAttribute('format', 'html');
                var answer = buildImgTag(String(rowValues[col]));
                //ordering has a simpler XML format than the other question types
                if (questionType == 'ordering') {
                    //feedback only supported for multiple choice, short answer, and truefalse
                    //Strip feedback from non-supported question types
                    answer = stripFeedback(answer);
                    var textElement = XmlService.createElement('text').setText(answer);
                    answerElement.addContent(textElement);
                } else {
                    var fraction = rowValues[++col];
                    //add additional attribute to answer element
                    answerElement.setAttribute('fraction', fraction);
                    var answerToUpper = answer.toUpperCase();
                    //if there is no feedback, put answer text in child text element of answer
                    if (answerToUpper.indexOf("[FEEDBACK]") == -1) {
                        var textElement = XmlService.createElement('text').setText(answer);
                        answerElement.addContent(textElement);
                    } else {
                        //insert feedback if present or question type is not ordering
                        //get text element containing answer text
                        var answerTextElement = getAnswerText(answer, questionType);
                        //make text element child of parent answer element
                        answerElement.addContent(answerTextElement);
                        //get feedback element containing child text element with feedback text
                        var feedbackElement = getFeedbackText(answer);
                        //make feedback element child of parent answer element
                        answerElement.addContent(feedbackElement);
                    }
                }
                questionParentElement.addContent(answerElement);
            }
        }
    }
}


function answertext(answer, questionType) {
    //allow for any case combination of [feedback]
    var answerToUpper = answer.toUpperCase();
    var feedbackLocation = answerToUpper.indexOf("[FEEDBACK]");
    var textAnswer = answer.substring(0, feedbackLocation).trim();

    //To ensure compatibility with moodle, TRUE and FALSE must be lowercase
    if (questionType == 'truefalse') {
        textAnswer = textAnswer.toLowerCase();
    }

    return textAnswer;
}

function feedbacktext(answer) {
    var answerToUpper = answer.toUpperCase();
    var feedbackLocation = answerToUpper.indexOf("[FEEDBACK]");
    var feedback = answer.substring(feedbackLocation + 10, answer.length).trim();
    return feedback;
}

//get answer text element
function getAnswerText(answer, questionType) {
    var textAnswer = answertext(answer, questionType);
    var textElement = XmlService.createElement('text').setText(textAnswer);
    return textElement;
}

//get feedback element with child text element
function getFeedbackText(answer) {
    var feedback = feedbacktext(answer);
    var feedbackElement = XmlService.createElement('feedback');
    var textElement = XmlService.createElement('text').setText(feedback);
    feedbackElement.addContent(textElement);
    return feedbackElement;
}

function buildMatchingQuestions(questionParentElement, rowValues) {

    var startCol = 3; //start at the question

    for (var col = startCol; col < columnHeaders.length; col = col + 2) {

        var header = String(columnHeaders[col]);
        var value = String(rowValues[col]);

        if (header === 'answer') {

            if (!/\S/.test(value)) {
                //if answer doesn't have non white space characters, we want to skip answer column AND fraction column
                col++;
                continue;
            }
            value = buildImgTag(value);
            //child subquestion element of parent question element
            var subquestionElement = XmlService.createElement('subquestion')
                .setAttribute('format', 'html');
            //child text element of subquestion element
            var subquestionTextElement = XmlService.createElement('text').setText(value);
            subquestionElement.addContent(subquestionTextElement);

            header = String(columnHeaders[col + 1]);
            value = String(rowValues[col + 1]);
            if (header === 'fraction') {
                //doesnt need to be checked for non white spaces, moodle accepts these as answers
                //child answer element of subquestion element
                var answerElement = XmlService.createElement('answer');
                var valueToUpper = value.toUpperCase();
                //Strip feedback from non-supported question types
                if (valueToUpper.indexOf("[FEEDBACK]") != -1) {
                    value = value.substring(0, valueToUpper.indexOf("[FEEDBACK]"));
                }
                //child text element of answer element
                var answerTextElement = XmlService.createElement('text').setText(value);
                answerElement.addContent(answerTextElement);

                //add answer element as child of sub question element
                subquestionElement.addContent(answerElement);
            }
            questionParentElement.addContent(subquestionElement);
        }
    }
}

function stripFeedback(answer) {
    var answerToUpper = answer.toUpperCase();
    //Strip feedback from non-supported question types
    if (answerToUpper.indexOf("[FEEDBACK]") != -1) {
        answer = answer.substring(0, answerToUpper.indexOf("[FEEDBACK]"));
    }
    return answer.trim();
}

function buildGroupQuestions(questionParentElement, rowValues, questionType) {

    var startCol = 3; //start at the question
    var correctAnswers = [];
    var answerOptions = [];
    var orderedAnswers = {};
    var groupList = [];
    var tagName = questionType == 'ddwtos' ? 'dragbox' : 'selectoption';

    //Order the answers by group number
    for (var col = startCol; col < columnHeaders.length; col++) {
        var value = String(rowValues[col]);
        var group = rowValues[++col];

        if (!/\S/.test(value)) {
            //if answer doesnt have non white space characters, we want to skip answer column AND fraction/group column
            continue;
        }

        if (orderedAnswers[group] === undefined) {
            orderedAnswers[group] = [];
            groupList.push(group);
        }

        orderedAnswers[group].push(value);
    }

    groupList.sort();
    //for each group
    for (var index in groupList) {
        var grouping = groupList[index];
        var groupingAnswers = orderedAnswers[grouping];
        //for each option in the group
        for (var index2 in groupingAnswers) {
            var answer = groupingAnswers[index2];
            //create tag elements that contain answer and group info
            var tagElement = XmlService.createElement(tagName);
            answer = stripFeedback(answer);
            //answer text element
            var textElement = XmlService.createElement('text').setText(answer);
            //group info element
            var groupElement = XmlService.createElement('group').setText(grouping);
            tagElement.addContent(textElement);
            tagElement.addContent(groupElement);

            //separate answer options into correct answers and the rest
            if (index2 == 0) {
                correctAnswers.push(tagElement);
            } else {
                answerOptions.push(tagElement);
            }
        }
    }

    //add correct answer options first as child elements
    for (var node in correctAnswers) {
        questionParentElement.addContent(correctAnswers[node]);
    }

    //add the rest of the answer options
    for (var node in answerOptions) {
        questionParentElement.addContent(answerOptions[node]);
    }
}

function reformatClozeQuestions(value) {
    value = String(value); //cast to a string in case the cell has been formatted as a number
    value = stripFeedback(value);
    var firstIndex = value.indexOf('^');
    var secondIndex = null;
    var offset = 0

    if(firstIndex == -1) { //if ^ not found
        // try [[
        firstIndex = value.indexOf('[[')
        if(firstIndex == -1) {
            return value;
        }
    }

    if(value[firstIndex] == '^') {
        secondIndex = value.indexOf('^', firstIndex + 1);
        offset = 1;
    } else {
        secondIndex = value.indexOf(']]', firstIndex + 2);
        offset = 2;
    }

    var updateAnswers = value.substring(firstIndex + offset, secondIndex);
    updateAnswers = updateAnswers.trim().split(/\s*,\s*/);

    //replace everything between the two indexes with newly converted string
    var substitute = '';


    if(updateAnswers.length > 1) {
        //MULTICHOICE: _s for shuffle
        substitute = '{1:MULTICHOICE_S:=' + updateAnswers[0].trim() + '#OK~' + updateAnswers.slice(1).join("#Wrong~") + "#Wrong}";

    } else {
        //SHORTANSWER
        substitute = '{1:SHORTANSWER:%100%' + updateAnswers[0].trim() + '}';
    }

    value = value.substring(0, firstIndex) + substitute + value.substring(secondIndex + offset);

    //repeat this function with the rest of the string
    value = reformatClozeQuestions(value);

    return value;
}

//create shuffle answer element and add to parent question element
function addShuffleSetting(questionParentElement, shuffle) {
    var text = '';
    if(shuffle) text = 'true';
    else text = 'false';
    var shuffleElement = XmlService.createElement('shuffleanswers').setText(text);
    questionParentElement.addContent(shuffleElement);
}

function addGraderInfoSetting(questionParentElement) {
    var graderElement = XmlService.createElement('graderinfo')
        .setAttribute('format', 'html');
    var textElement = XmlService.createElement('text');
    graderElement.addContent(textElement);
    questionParentElement.addContent(graderElement);
}

function addDefaultSettings(questionParentElement, questionType) {
    if (typeof defaultSettings[questionType] !== 'undefined') {
        for (var setting in defaultSettings[questionType]) {
            var settingsElement = XmlService.createElement(setting).setText(defaultSettings[questionType][setting]);
            questionParentElement.addContent(settingsElement);
        }
    }
}

function addVideo(questionText) {

    // catches spreadsheet video tag
    regex = /\[VIDEO\s(.*?)(?:\s)?(\w*?)\s(\d*)\|(\d*)\]/;

    while(regex.test(questionText) == true) {

        var toReplace = questionText.match(regex)[0];
        var title = questionText.match(regex)[1];
        var hash = questionText.match(regex)[2];
        var width = questionText.match(regex)[3];
        var height = questionText.match(regex)[4];

        var newString = '<a href="http://kaf.ccle.ucla.edu/browseandembed/index/media/entryid/' + hash +
        '/showDescription/false/showTitle/false/showTags/false/showDuration/false/showOwner/false/showUploadDate/' +
        'false/playerSize/' + width + 'x' + height + '/playerSkin/26365392/">tinymce-kalturamedia-embed||' + title + 
        '||' + width + '||' + height + '</a>'

        // replace video tag with HTML code containing the same information
        questionText = questionText.replace(regex, newString);
    }

        return(questionText);

}
