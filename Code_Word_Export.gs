/*
(QB)² - Question Bank Quick Builder

The Clear BSD License

Copyright (c) 2021 Regents of the University of California
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted (subject to the limitations in the disclaimer
below) provided that the following conditions are met:

     * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

     * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

     * Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/** function plainTextExport() {
    showCompilingMessage('Compiling Sheet as Text file...', 'This may take a few minutes...', 350);

    if (exportSpreadsheetPlaintext(false, null)) {

        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
            'Export Complete',
            'Please check the developer logs to see the output.',
            ui.ButtonSet.OK);
        return;
    }
} */

//Creates a Google Doc with the questions, then provides an export link as
//.docx.
function docxExport() {
    showCompilingMessage('Compiling Sheet as .docx file...', 'This may take a few minutes...', 350);
    generateResult(false);
}

//Creates a Google Doc with the questions, then provides an export link as
//.pdf.
function pdfExport() {
    showCompilingMessage('Compiling Sheet as .pdf file...', 'This may take a few minutes...', 350);
    generateResult(true);

}

//Creates a Google Doc and converts it to a pdf if isPDF is true, or .docx
//otherwise.
function generateResult(isPDF) {
    var results = createDocument(SpreadsheetApp.getActive().getName() + " - Export", true);
    var docID = results[0];
    var folderID = results[1];
    var doc = DocumentApp.openById(docID);
    createHeaderAndFooter(doc);
    exportSpreadsheetPlaintext(doc.getBody().setPageHeight(792).setPageWidth(612));
    handleFeedback(doc.getBody()); // parses HTML
    doc.saveAndClose();
    displayResult(doc, folderID, isPDF);
}

//Prints the spreadsheet.  Uses the Google doc specified by body
//Returns true if the export was successful.
function exportSpreadsheetPlaintext(body) {
    //We export from the active sheet, but this is undesirable if the active sheet is not a question bank
    //Let the user know they need to change sheets if they are on one of the following
    if (activeSheetName == 'Categories' || activeSheetName == 'QuestionTypes' || activeSheetName == 'QuestionOptions' || activeSheetName == 'Import Errors') {
        var ui = SpreadsheetApp.getUi();
        var result = ui.alert(
            'Switch to Question Bank',
            'Export is performed on the sheet that is currently visible.\nPlease navigate to your question bank and try again.',
            ui.ButtonSet.OK);
        return;
    }
    var spreadsheet = SpreadsheetApp.getActive();
    var activeSheetName = getActiveSheetName();
    var sheet = spreadsheet.getSheetByName(activeSheetName);

    var range = sheet.getDataRange();
    var values = range.getValues();
    var numRows = range.getNumRows();

    for (var r = 2; r < numRows; r++) {
        if(values[r][0] == "") {
            values[r][0] = values[r-1][0];
        }
    }

    //Sort the array alphabetically by category to group together questions efficiently
    values.sort(categorySort);

    var prevCategory = "";

    try {
        //Catch errors at the sheet level to provide a popup letting the user know export failed.
        //r = 2 because first two rows are reserved for column header and extra space for notes that arent needed for compiling.
        for (var r = 2; r < numRows; r++) {
            try {
                //Catch errors at the question level so we can provide users with the line they occur on

                printCategory(String(values[r][0]), prevCategory, r, body);
                prevCategory = String(values[r][0]);

                printQuestion(values[r], body);

            } catch (e) {
                //throw "An error occurred while processing the question in row " + (r + 1) + " that prevented export from completing. " +
                //    "Try removing this question and exporting again.";
                Logger.log("" + e);
                throw e;
            }
        } // End row for loop

    } catch (e) {
        e = e.toString(); //Cast to string in case the exception is not of type string
        var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' + e;
        //Variable height based on error length
        var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(50 + 15 * Math.ceil((e.length - 67) / 67));
        SpreadsheetApp.getUi().showModalDialog(html, 'Plaintext Export Failed');
        return false;
    }
    return true;
}

//Prints out the category name if needed
//Will not print it category and prevCategory match, unless it is the first row
function printCategory(category, prevCategory, rowNumber, body) {
    // If empty category at the start (row 2), give it an empty category
    if (category == '' && prevCategory == '' && rowNumber == 2) {
        body.appendParagraph("Uncategorized:")
            .setHeading(DocumentApp.ParagraphHeading.HEADING2)
            .setBold(false);
    } else if (category != prevCategory) {
        body.appendParagraph("Category - " + category + ":")
            .setHeading(DocumentApp.ParagraphHeading.HEADING2);
    } else {
        body.appendParagraph("");
    }
}

//Prints out an individual question.
//values is an array corresponding to the question's row
//Prints differently for plaintext and .docx
function printQuestion(values, body) {
    // Get question type
    var questionType = String(values[1]);

    //question answer options information
    //questionTypeElement is the parent element of all the answer option elements
    switch (questionType) {
        case "essay":
        case "description":
            body.appendParagraph(values[2]).setBold(false);
            break;
        case "poodllrecording":
            body.appendParagraph(values[2] + "\n(Expects an oral response)").setBold(false);
            break;
        case 'multichoice':
        case 'truefalse':
            printMultiTrueFalseQuestion(values, body);
            break;
        case 'shortanswer':
            printShortAnswerQuestion(values, body);
            break;
        case 'ordering':
            printOrderingQuestion(values, body);
            break;
        case 'matching':
            printMatchingQuestion(values, body);
            break;
        case 'dragdrop':
        case 'missingwords':
            printGroupQuestion(values, body);
            break;
        case 'cloze':
            printClozeQuestion(values, body);
            break;
        case 'multianswer':
            printMultianswerQuestion(values, body);
            break;
        default:
            body.appendParagraph("Unsupported question type: " + questionType).setBold(false);
            break;
    }
}

//Prints a multichoice or a truefalse question
//values is an array corresponding to the question row
function printMultiTrueFalseQuestion(values, body) {
    body.appendParagraph(values[2]).setBold(false);
    var listID = null;
    for (var c = 3; c < values.length - 1; c += 2) {
        if (String(values[c]) != '') {
            //97 = a
            var credit = String(values[c + 1]);
            var correctText = "";
            if (credit == '') {
                correctText = " (No credit specified)";
            } else if (credit == '100') {
                correctText = " (Correct)";
            } else if (parseInt(credit, 10) > 0) {
                correctText = " (Partially Correct - " + values[c + 1] + "% credit)";
            }
            var answer = body.appendListItem(values[c] + correctText)
                            .setGlyphType(DocumentApp.GlyphType.LATIN_LOWER);;
            if (credit == '100') {
                answer.setBold(true);
            } else {
                answer.setBold(false);
            }
            if (listID == null) {
                listID = answer;
            } else {
                answer.setListId(listID);
            }
        }
    }
}

//Prints a shortanswer question
//values is an array corresponding to the question row
function printShortAnswerQuestion(values, body) {
    body.appendParagraph(values[2]).setBold(false);
    body.appendParagraph("The following answers are correct:");
    var listID = null;
    var hasPartialCredit = false;
    for (var c = 3; c < values.length - 1; c += 2) {
        if (String(values[c]) != '') {
            //TODO: Add better error checking
            var credit = parseInt(String(values[c + 1]), 10);
            if (credit == 100) {
                var answer = body.appendListItem(values[c])
                                .setGlyphType(DocumentApp.GlyphType.BULLET);
                if (listID == null) {
                    listID = answer;
                } else {
                    answer.setListId(listID);
                }
            } else if (credit > 0 && credit < 100) {
                hasPartialCredit = true;
            }
        }
    }
    if (hasPartialCredit) {
        body.appendParagraph("The following answers are partially correct:");
        listID = null;
        for (var c = 3; c < values.length - 1; c += 2) {
            if (String(values[c]) != '' && String(values[c + 1]) != '') {
                var creditAsInt = parseInt(String(values[c + 1]), 10);
                if (creditAsInt > 0 && creditAsInt < 100) {
                    var answer = body.appendListItem(values[c] + " (" + creditAsInt + "% credit)")
                                    .setGlyphType(DocumentApp.GlyphType.BULLET);
                    if (listID == null) {
                        listID = answer;
                    } else {
                        answer.setListId(listID);
                    }
                }
            }
        }
    }

    //TODO: support feedback
}

//Prints an ordering question
//values is an array corresponding to the question row
function printOrderingQuestion(values, body) {
    body.appendParagraph(values[2]).setBold(false);
    body.appendParagraph("The correct order is as follows:");
    var listID = null;
    for (var c = 3; c < values.length; c += 2) { // increment by 2 to avoid gray cells.
        if (String(values[c]) != '') {
            var answer = body.appendListItem(values[c])
                            .setGlyphType(DocumentApp.GlyphType.BULLET);
            if (listID == null) {
                listID = answer;
            } else {
                answer.setListId(listID);
            }
        }
    }
}

//Prints a matching question
//Values is an array corresponding to the question row
function printMatchingQuestion(values, body) {
    body.appendParagraph(values[2]).setBold(false);
    body.appendParagraph("The correct pairs are as follows:");
    var listID = null;
    for (var c = 3; c < values.length - 1; c += 2) {
        if (String(values[c]) != '' && String(values[c + 1]) != '') {
            var answer = body.appendListItem(values[c] + " ===> " + values[c + 1])
                            .setGlyphType(DocumentApp.GlyphType.BULLET);
            if (listID == null) {
                listID = answer;
            } else {
                answer.setListId(listID);
            }
        }
    }
}

//Prints a dragdrop or missingwords question
//Values is an array corresponding to the question row
function printGroupQuestion(values, body) {
    var paragraph = body.appendParagraph("").setBold(false);
    var groups = [];

    //Get the correct answers for display in the question
    var question = values[2];
    for (var c = 4; c < values.length - 1; c += 2) {
        if (String(values[c]) != '') {
            var group = parseInt(String(values[c]), 10);
            if (groups.indexOf(group) == -1) {
                groups.push(group);
            }
            question = question.replace("[[" + group + "]]", "[[" + values[c - 1] + " (from group " + group + ")]]");
        }
    }
    var bracketIndex = question.indexOf("[[");
    while (bracketIndex != -1) {
        paragraph.appendText(question.substr(0, bracketIndex)).setUnderline(false);
        question = question.substr(bracketIndex, question.length);

        //TODO: Better error checking
        var bracketEnd = question.indexOf("]]");
        var answer = question.substr(2, bracketEnd - 2);
        paragraph.appendText(answer).setUnderline(true);

        question = question.substr(bracketEnd + 2, question.length);
        bracketIndex = question.indexOf("[[");
    }
    paragraph.appendText(question + "\n").setUnderline(false);

    groups.sort();
    for (var ii = 0; ii < groups.length; ii++) {
        var listID = null;
        body.appendParagraph("Answer choices for group " + groups[ii] + ":");
        for (var c = 4; c < values.length - 1; c += 2) {
            if (values[c] == groups[ii] && String(values[c - 1]) != '') {
                var answer = body.appendListItem(values[c - 1])
                                .setGlyphType(DocumentApp.GlyphType.BULLET);
                if (listID == null) {
                    listID = answer;
                } else {
                    answer.setListId(listID);
                }
            }
        }
    }
}

//Prints a cloze question
//Values is an array corresponding to the question row
function printClozeQuestion(values, body) {
    var paragraph = body.appendParagraph("").setBold(false);
    var groups = [];

    //Get the correct answers for display in the question
    var question = values[2];
    //Replace brackets with carets
    question.replace(/(\[\[|\]\])/g, '^');
    var caretIndex = question.indexOf("^");
    var groupCount = 1;
    while (caretIndex != -1) {
        paragraph.appendText(question.substr(0, caretIndex)).setUnderline(false);
        question = question.substr(caretIndex + 1, question.length);

        //TODO: Better error checking
        var caretEnd = question.indexOf("^");
        var answer = question.substr(0, caretEnd);
        if (answer.indexOf(",") == -1) {
            //shortanswer
            paragraph.appendText(answer).setUnderline(true);
        } else {
            //multichoice
            var allAnswers = answer.split(",").map(function(item) {
                                                return item.trim();
                                            });
            paragraph.appendText(allAnswers[0] + " (from group " + groupCount + ")").setUnderline(true);
            groupCount++;
            groups.push(allAnswers);
        }

        question = question.substr(caretEnd + 1, question.length);
        caretIndex = question.indexOf("^");
    }
    paragraph.appendText(question + "\n").setUnderline(false);

    for (var ii = 0; ii < groups.length; ii++) {
        var listID = null;
        body.appendParagraph("Answer choices for group " + (ii + 1) + ":");
        for (var jj = 0; jj < groups[ii].length; jj++) {
            if (groups[ii][jj] != '') {
                var answer = body.appendListItem(groups[ii][jj])
                                .setGlyphType(DocumentApp.GlyphType.BULLET);
                if (listID == null) {
                    listID = answer;
                } else {
                    answer.setListId(listID);
                }
            }
        }
    }
}

//Prints a multianswer question
//values is an array corresponding to the question row
function printMultianswerQuestion(values, body) {
    body.appendParagraph(values[2]).setBold(false);
    body.appendParagraph("Selecting the following answers gives positive credit:");
    var listID = null;
    var hasNegativeCredit = false;
    for (var c = 3; c < values.length - 1; c += 2) {
        if (String(values[c]) != '') {
            //TODO: Add better error checking
            var credit = parseInt(String(values[c + 1]), 10);
            if (credit > 0) {
                var answer = body.appendListItem(values[c] + " (" + credit + "% credit)")
                                .setGlyphType(DocumentApp.GlyphType.BULLET);
                if (listID == null) {
                    listID = answer;
                } else {
                    answer.setListId(listID);
                }
            } else {
                hasNegativeCredit = true;
            }
        }
    }
    if (hasNegativeCredit) {
        body.appendParagraph("The following answers are incorrect:");
        listID = null;
        for (var c = 3; c < values.length - 1; c += 2) {
            if (String(values[c]) != '' && String(values[c + 1]) != '') {
                var creditAsInt = parseInt(String(values[c + 1]), 10);
                if (creditAsInt <= 0) {
                    var answer = body.appendListItem(values[c] + (creditAsInt == 0 ? "" : " (" + Math.abs(creditAsInt) + "% penalty for selection)"))
                                    .setGlyphType(DocumentApp.GlyphType.BULLET);
                    if (listID == null) {
                        listID = answer;
                    } else {
                        answer.setListId(listID);
                    }
                }
            }
        }
    }

    //TODO: support feedback
}

//creates a Google Doc file with the given name, moving it to the same
//folder as the current spreadsheet.
//Returns the created file's ID and the ID of the folder it is in.
function createDocument(filename, replaceFile) {
    //Creates the Google Doc and moves it into the same folder as the original file
    var file = DocumentApp.create(filename);
    var fileID = file.getId();
    //change file from a Document to a File so we can move it
    file = DriveApp.getFileById(fileID);

    var currentFileId = SpreadsheetApp.getActive().getId();
    var currentFiles = DriveApp.getFilesByName(SpreadsheetApp.getActive().getName());
    var currentFile;

    while (currentFiles.hasNext()) {

        currentFile = currentFiles.next();

        if (currentFile.getId() == currentFileId) break;
    }

    var folders = currentFile.getParents();

    var parentFolder;
    while (folders.hasNext()) {
        parentFolder = folders.next();
    }

    if (parentFolder != null) {
        var newFile;

        if (replaceFile) {

            newFile = file.makeCopy(file.getName(), parentFolder);
            file.setTrashed(true);

            currentFiles = parentFolder.getFiles();

            while (currentFiles.hasNext()) {
                currentFile = currentFiles.next();
                if (currentFile.getName() == newFile.getName() && currentFile.getId() != newFile.getId()) {
                    currentFile.setTrashed(true);
                }
            }
        } else {
            newFile = file.makeCopy(file.getName(), parentFolder);
            file.setTrashed(true);
        }

        return [newFile.getId(), parentFolder.getId()];
    } else {
        //The user does not own the spreadsheet, so creating the document in their home folder.
        return [file.getId(), null];
    }
}

//Adds a default header and footer to the created Google Doc.
function createHeaderAndFooter(doc) {
    //TODO: add footer with page number

    var body = doc.getBody();
    body.appendParagraph(doc.getName())
      .setHeading(DocumentApp.ParagraphHeading.HEADING1)
      .setAlignment(DocumentApp.HorizontalAlignment.CENTER);
}

//Convert the file and display the result to the user.
function displayResult(doc, folderID, isPDF) {
    var file = null;

    if (!isPDF) {
        file = convertToDocx(doc, folderID);
    } else {
        file = convertToPdf(doc, folderID);
    }

    var htmlText = '<link rel="stylesheet" href="https://ssl.gstatic.com/docs/script/css/add-ons1.css">' +
        '<style>.display { width:355px; height:85px; text-align: center; overflow: auto; } </style>' +
        'File exported successfully. You can view the file here:' +
        '<div class="display"><br><br><a href="' + file.getUrl() + '" target="_blank">' + file.getName() + '</a></div>';

    var html = HtmlService.createHtmlOutput(htmlText).setWidth(400).setHeight(100);

    SpreadsheetApp.getUi().showModalDialog(html, (isPDF ? 'PDF Export Complete!' : 'Word (.docx) Export Complete!'));
}

//Converts the given Google Doc to a .docx file in the specified folder.
function convertToDocx(doc, folderID) {
    var docAsFile = DriveApp.getFileById(doc.getId());
    var url = "https://docs.google.com/document/d/" + doc.getId() + "/export?format=docx";
    var urlBlob = UrlFetchApp.fetch(url, {
            "headers" : {Authorization: 'Bearer ' + ScriptApp.getOAuthToken()},
            "muteHttpExceptions" : true });
    urlBlob = urlBlob.getBlob().setName(docAsFile.getName() + ".docx");

    var file = null;
    if (folderID == null) {
        file = DriveApp.createFile(urlBlob);
    } else {
        file = DriveApp.getFolderById(folderID).createFile(urlBlob);
    }

    return file;
}

//Converts the given Google Doc to a .pdf file in the specified folder.
function convertToPdf(doc, folderID) {
    var docAsFile = DriveApp.getFileById(doc.getId());
    var docBlob = doc.getAs('application/pdf').setName(docAsFile.getName() + ".pdf");
    var file = null;
    if (folderID == null) {
        file = DriveApp.createFile(docBlob);
    } else {
        file = DriveApp.getFolderById(folderID).createFile(docBlob);
    }

    return file;
}

//Sorts a 2D array of questions by the category column (index 0)
function categorySort(a, b) {
    var left = (String)(a[0]);
    var right = (String)(b[0]);

    if (left === right) {
        return 0;
    }
    else {
        return (left < right) ? -1 : 1;
    }
}

// parses HTML before the document is exported
function handleFeedback(body) {

    // two regexes are needed because findText() takes a string regex instead of a regex variable.
    // each element in the arrays represent a different tag that can be found
    var regexString = ["\[[Ff][Ee][Ee][Dd][Bb][Aa][Cc][Kk]\]"];
    var regex = [/(.*)?(\[[Ff][Ee][Ee][Dd][Bb][Aa][Cc][Kk]\])(.*)?/];
    // for each tag, search for an instance of the tag until they are all removed and turned into formatting.
    for(var i = 0; i < regexString.length; i++) {

        var noTags = false; // flag to end loop if nothing is found

        while(!noTags) {

            // this returns the element of the text that needs to be fixed.
            // unfortunately, formatting changes can only be applied to the entire element, not just a subset of it.
            var textRangeElement = body.findText(regexString[i]);

            if(textRangeElement == null) {
                noTags = true;
                break;
            }

            var textToFix = textRangeElement.getElement();
            var textString = textToFix.getText();

            var match = textString.match(regex[i]);

            // capture the text before and after the tags as well
            var beforeMatch = match[1].trim();
            var matchText = match[2];
            var afterMatch = match[3].trim();

            var newString = beforeMatch + " [Question Feedback: " + afterMatch + "]";

            textToFix.setText(newString);

        }

    }

}